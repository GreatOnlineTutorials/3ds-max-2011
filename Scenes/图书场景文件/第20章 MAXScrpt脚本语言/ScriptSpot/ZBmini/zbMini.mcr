--when using pick buttons, go into scene object selection mode?

macroscript miniZB category:"stuh505" buttonText:"miniZB" tooltip:"Mini ZB"
(
	local detail
	local obj
	local vedgecache=#()
	local mindist
	local error
	local base
	local baseSubd
	local omod
	local lastTex
	local canMap = false
	local dpsubpix
	local dim
	local finalFaceCount
		
	struct vpos
	(
		index,
		pos
	)
	
	fn nearestPowerOf2 n=
	(
		--if 1 above, round up
		if (pow 2 (ceil ((log10 (n-1))/(log10 2))))==(n-1) then
			return (pow 2 (ceil ((log10 n)/(log10 2))))
		
		--otherwise, round down
		return (pow 2 (floor ((log10 n)/(log10 2))))
	)
	
	fn nearestSquare n=
	(
		--if 1 above, round up
		if (pow (floor (sqrt (n-1))) 2)==(n-1) then
			return (pow (ceil (sqrt n)) 2)
		
		--round down
		return (pow (floor (sqrt n)) 2)
	)
		
	rollout miniZB "Mini ZB" width:147 height:429
	(
		bitmap bmp1 "Bitmap" pos:[-2,-1] width:149 height:72 fileName:"zbmini1.bmp"
		groupBox grpDetail "Model Control" pos:[5,81] width:133 height:96
		button bZoom "Zoom In" pos:[19,202] width:106 height:23 enabled:false
		groupBox grpSubdivision "Subdivision Modelling" pos:[7,183] width:132 height:94
		button bIncrSub "+" pos:[74,229] width:43 height:19 enabled:false
		button bDecrSub "-" pos:[25,229] width:44 height:18 enabled:false
		groupBox grpTex "Texture Projection" pos:[6,282] width:133 height:141
		spinner spnMapDim "Map Dim" pos:[27,300] width:98 height:16 range:[256,4096,512] type:#integer enabled:false
		button bCalcDisp "Calc Dispmap" pos:[18,393] width:101 height:20 enabled:false
		button bCalcNormal "Calc Normalmap" pos:[18,367] width:101 height:20 enabled:false
		button bSwitchLevel "Grab Base Mesh" pos:[13,146] width:111 height:22 enabled:false
		button bSubdivLocal "Subdiv Local" pos:[28,253] width:84 height:19 enabled:false
		button bDrop "Drop" pos:[93,119] width:31 height:22 enabled:false
		spinner spnDPSubPix "DPSubPix" pos:[27,322] width:98 height:16 range:[0, 4, 2] type:#integer enabled:false
		spinner spnMinDispv "Min DispV" pos:[27,343] width:98 height:16 range:[1,100, 10] type:#integer enabled:false
		--label lblPPF "" pos:[13,345] width:50 height:16
		button bPickBase "Pick Base" pos:[10,98] width:57 height:20
		button bPickTop "Pick Top" pos:[70,98] width:57 height:20
		checkbox chkAutoUVW "Auto UVW" pos:[11,123] width:80 height:16 checked:false
		
		fn toggleBaseDepButtons=
		(
			newVal = (not bZoom.enabled)
			bZoom.enabled = newVal
			bCalcNormal.enabled= newVal
			bCalcDisp.enabled = newVal
			bDrop.enabled = newVal
			bSwitchLevel.enabled = newVal
			bIncrSub.enabled = newVal
			bDecrSub.enabled = newVal
			bSubdivLocal.enabled = newVal
			spnMapDim .enabled = newVal
			spnDPSubPix.enabled = newVal
			spnMinDispv.enabled = newVal
			canMap = newVal
		)
			
		fn predictSubdivFaces=
		(
			--calculations to determine pre-subdividing
			dim = spnMapDim.value
			
			--dpsubpix = (ceil ((log10 (dim*dim*0.6/(numfaces*pixPerFace)))/(log10 4))) as integer
			finalFaceCount = (base.numFaces*(pow 4 spnDPSubPix.value)) as integer
			pixelsPerFace = dim*dim/finalFaceCount
	
			lblPPF.text = ((pixelsPerFace as string) + " ppf")
		)
		
		fn autoUnwrap=
		(
			suspendEditing()
			try
			(
				addModifier base (unwrap_uvw())
				select base
				forceCompleteRedraw()
				base.unwrap_uvw.flattenMapNoParams()
				forceCompleteRedraw()
								
				base.unwrap_uvw.pack 0 0.02 true false false
				convertToPoly(base)
				
			) catch
				resumeEditing()
			resumeEditing()
		)
	
		fn restoreEdge=
		(
			for v in vedgecache do
			(
				if v.index <= detail.vertices.count then
					detail.vertices[v.index].pos = v.pos
				else
					error = true
			)
		)
		
		fn findMinDist =
		(
			select detail	
			subObjectLevel = 2
			actionMan.executeAction 0 "40021"  -- Selection: Select All
			detail.convertSelectionToBorder 2 1
			
			mindist = 999
			for v in detail.selectedVertices do
			(
				for v2 in detail.selectedVertices do
				(
					if (v.index != v2.index) then
					(
						d = (distance v.pos v2.pos)
						if d < mindist then
							mindist = d
					)
				)
			)
			return mindist
		)
		
		fn initDetail=
		(
			--calc threshold (/2 for safety)
			obj.weldThreshold = (findMinDist()/2)
			
			detail.paintDeformSize = mindist*4
			detail.paintDeformAxis = 1 --original, 2 is deformed
			detail.paintDeformValue = mindist
			
			--setup cache for edge protection
			--ASSERT: border verts already selected
			vedgecache = #()
			for v in detail.selectedVertices do
				append vedgecache (vpos index:v.index pos:v.pos)
				
			detail.selectedVertices = #()
			subObjectLevel = 1
		)
		
		fn incrIterations n=
		(
			obj = $
			if ((classOf obj) == Editable_Poly) then
			(
				if (obj.surfSubdivide == false) then
					obj.iterations = 0
					
				obj.surfSubdivide = true
				obj.subdivSmoothing = false
				obj.isolineDisplay = false
				obj.showCage = false
							
				obj.iterations = (obj.iterations + n)
				
				if obj.iterations < 0 then
					obj.iterations = 0
			)
		)
	
	
		fn enterDetail=
		(
				obj = $
				
				if ((classOf obj) != Editable_Poly) then
						convertToPoly(obj)
				
				if (obj != undefined and (classOf obj) == Editable_Poly and obj.selectedFaces.count > 0) then
				(
					bZoom.caption = "Zoom Out"
					bSwitchLevel.enabled = false
					bIncrSub.enabled = false
					bDecrSub.enabled = false
					bCalcNormal.enabled = false
					bCalcDisp.enabled = false
					
					omod = undefined
					
					if (obj.iterations > 0) then
						convertToPoly(obj)
					
					polyOp.detachFaces obj obj.selectedFaces asNode:true name:"DETAIL"
					detail = (getNodeByName "DETAIL")
		
					select #(detail, obj)
					actionMan.executeAction 0 "281"  -- Tools: Hide Unselected
					
					initDetail()
					
					omod = optimize()
					omod.facethreshold1 = 6.0
					addModifier obj omod
					freeze obj
				) 
		)
		
		fn leaveDetail=
		(
			
				if (omod != undefined) then
					deleteModifier obj omod
				unfreeze obj
				
				maxOps.CollapseNode detail true
				
				restoreEdge()
				
				--actionMan.executeAction 0 "277"  -- Tools: Unhide All
				unhide obj
				
				--attach detail segment
				obj.attach detail detail
		
				--select all the borders
				select obj
				subObjectLevel = 2
				actionMan.executeAction 0 "40021"  -- Selection: Select All
				obj.convertSelectionToBorder 2 1
				
				--weld borders
				obj.EditablePoly.weldFlaggedVertices()
				
				--fix display
				subObjectLevel = 4
				obj.selectedFaces = #()
				
				bSwitchLevel.enabled = true
				bIncrSub.enabled = true
				bDecrSub.enabled = true
				bCalcNormal.enabled = canMap
				bCalcDisp.enabled = canMap
			
			
			if (error == true) then
			(
				error = false
				messageBox "I was not able to patch your model back together properly because you removed some of the border edges!"
			)
		)
	
		on bZoom pressed do
		(
			if bZoom.caption == "Zoom In" then
			(
				if obj.selectedFaces.count == 0 then
					messageBox "Select some faces first"
				else
					enterDetail()
			) else
			(
				leaveDetail()
				bZoom.caption = "Zoom In"
			)
		)
			
		on bIncrSub pressed do
		(
			incrIterations(1)
		)
		
		on bDecrSub pressed do
		(
			incrIterations(-1)
		)
			
		on bPickBase pressed do
		(
			if base != undefined then
				delete base
			base = $
			
			if (chkAutoUVW.checked == true) then
				autoUnwrap() --converts to poly
			
			if (classOf base != "Editable_Poly") then
				convertToPoly(base)
			
			hide base
			if obj != undefined then
				delete obj
			obj = (copy base)
			select obj
				
			messageBox "Base mesh stored.  Now operating on top level mesh."
			
			toggleBaseDepButtons()
			
			--predictSubdivFaces()
			
		)
		
		on bPickTop pressed do
		(
			if obj != undefined then
				delete obj
			
			obj = $
			if (classOf obj != "Editable_Poly") then
				convertToPoly(obj)
			
			base.center = obj.center			
			
		)
		
		on bSwitchLevel pressed do
		(
			if (bSwitchLevel.caption == "Grab Base Mesh") then
			(
				if obj != undefined then
					hide obj
				if base != undefined then
					unhide base
				select base
				
				bSwitchLevel.caption = "Grab Top Mesh"
			) else if bSwitchLevel.caption == "Grab Top Mesh" then
			(
				if base != undefined then
					hide base
				if obj != undefined then
					unhide obj
				select obj
				
				bSwitchLevel.caption = "Grab Base Mesh"
			)
		)--endbSwitchLevel pressed
		
				
		on  bSubdivLocal pressed do
		(
			polyOp.tessellateByFace $ $.selectedFaces
		)
		
		fn bakeDisp dim=
		(
			objName = base.name
			path = ((getdir #image) + "\\")
					
			unhide baseSubd
			unhide obj
			
			obj.center = baseSubd.center
			
			fname = (path + objName + "_DispMap.bmp")
				
			--setup the bake element
			be = HeightMap()
			be.outputSzX=dim
			be.outputSzY=dim
			be.fileType = (path + objName + "_DispMap.bmp") --must be set to full file path
			be.targetMapSlotName = "Displacement"
			be.filterOn = true
			
			--add bake element
			bi = baseSubd.INodeBakeProperties
			bi.addBakeElement be
			bi.bakeChannel=1
			bi.bakeEnabled = true	
			
			--projection
			p = projection()
			addModifier baseSubd p
			baseSubd.INodeBakeProjProperties.projectionMod = p
			p.addObjectNode obj	
			
			--bake properties
			pj = baseSubd.INodeBakeProjProperties
			pj.BakeSubObjLevels = false
			pj.normalSpace = #tangent
			pj.projectionModTarget = "Object Level"
	
			pj.bakeObjectLevel = true
			pj.useObjectBakeForMtl = true
			pj.hitresolvemode = #furthest
			pj.rayoffset = 10
			pj.usecage = false
			pj.proportionaloutput = false
			pj.hitworkingmodel = false
			pj.heightMapMin = -100.0
			pj.heightMapMax = 100.0
			pj.enabled = true
			
			--first pass to determine range
			select baseSubd
			renderers.current = Default_Scanline_Renderer()
			abmp = bitmap dim dim
			pj.projSpace = #raytrace --to get buffer
			render rendertype:#bakeSelected outputwidth:dim outputheight:dim  vfb:false to:abmp
			
			pj.heightMapMin = pj.heightbufmin
			pj.heightMapMax = pj.heightbufmax
			minbuffer = pj.heightbufmin
			maxbuffer = pj.heightbufmax			
			
			--second pass to get correct
			pj.projSpace = #uvw_match
			render rendertype:#bakeSelected outputwidth:dim outputheight:dim  vfb:false to:abmp			
			--compute offset and disp value for same proportions

			--disp = max{dispV*pixelV/(28.845*255), 20} m   , cuttoff therefore is at 576.9 dispV
			
			
			boundingSize = (distance base.max base.min)
			MRf = 255*0.925^boundingSize+10
			
			range = (maxbuffer - minbuffer)
			minDispV = spnMinDispV.value

			minDP = 0
			maxDP = 255
			
			if ((range*MRf) < minDispV ) then
			(
				lowerBoundWarning = true
				
				scaler = range*MRf/minDispV
			
				--scale down pixel range and offset middle to correct middle point
				minDP = (minDP*scaler + 127.5 - 127.5*scaler) as integer
				maxDP = (maxDP*scaler + 127.5 - 127.5*scaler) as integer
			)
			
			offset = ((maxDP*minbuffer - minDP*maxbuffer)/(255*(maxbuffer-minbuffer)))
			dispval = (range*MRf*255/(maxDP-minDP))
			
			if (maxbuffer > 20 or minbuffer < 20) then
				uppderBoundWarning = true
			
			--output notes to user
			output = newScript()
			format "zbMini displacement info for % \n" objName to:output
			format "minimum height used:\t%\n" minbuffer to:output
			format "maximum height used:\t%\n" maxbuffer to:output
			format "height range :\t%\n" range to:output
			format "pixel value of maximum height:\t%\n" maxDP to:output
			format "pixel value of minimum height:\t%\n" minDP to:output
			format "offset:\t%\n" offset to:output
			format "disp value:\t%\n" dispval to:output
			
			if (lowerBoundWarning == true) then
				format "NOTIFICATION: not using full range of bitmap because displacement value would be too small \n" to:output
			if (upperBoundWarning == true) then
				format "WARNING: Displacement value exceeds Mental Ray's default limit (20m) \n" to:output
			
			dispTex = bitmapTexture filename:fname name:"Disp Tex"
			dispTex.coords.mapChannel = bi.bakeChannel
			dispTex.output.RGB_Offset = offset
			
			--get rid of subdivided base...
			delete baseSubd
			
			if (((classOf base.material) as string) !="Standardmaterial") then
				base.material = standardMaterial()
					
			base.material.displacementMap = dispTex
			base.material.displacementMapAmount = dispval
			
			hide obj
			unhide base
			bSwitchLevel.caption = "Grab Top Mesh"
		)
		
		fn bakeNormal dim=
		(
		
			objName = base.name
			path = ((getdir #image) + "\\")
					
			unhide base
			unhide obj
			
			obj.center = base.center
			
			---------------------------
			fname = (path + objName + "_NormalMap.bmp")
				
			--setup the bake element
			be = NormalsMap()
			be.outputSzX=dim
			be.outputSzY=dim
		
			be.fileType= fname --must be set to full file path
			be.targetMapSlotName = "Bump"
			be.useNormalBump = true
			be.filterOn = true
			---------------------------
			
			--add bake element
			bi = base.INodeBakeProperties
			bi.addBakeElement be
			bi.bakeChannel=1
			bi.bakeEnabled = true	
			
			--projection
			p = projection()
			addModifier base p
			base.INodeBakeProjProperties.projectionMod = p
			p.addObjectNode obj
			select base
			--p.autowrapCage()
			--p.pushCage 3.0		
			
			--bake properties
			pj = base.INodeBakeProjProperties
			pj.BakeSubObjLevels = false
			pj.normalSpace = #tangent
			pj.projectionModTarget = "Object Level"
	
			pj.projSpace = #uvw_match
	
			pj.bakeObjectLevel = true
			pj.useObjectBakeForMtl = true
			pj.hitresolvemode = #furthest
			pj.rayoffset = 10
			pj.usecage = true
			pj.proportionaloutput = false
			pj.hitworkingmodel = false
			pj.enabled = true
			
			--bake texture
			select base
			renderers.current = Default_Scanline_Renderer()
			abmp = bitmap dim dim
			render rendertype:#bakeSelected outputwidth:dim outputheight:dim  vfb:false to:abmp			
			------------------------------------
			--make tex for normal map
			theNormalTex = bitmapTexture filename:fname name:"Normal Tex"
			lastTex = theNormalTex
			theNormalTex.coords.mapChannel = bi.bakeChannel
				
			--make normal bump tex
			normalBumpTex = Normal_Bump normal_map:theNormalTex
			normalBumpTex.method = 0 --tanget
			
			if (((classOf base.material) as string) !="Standardmaterial") then
				base.material = standardMaterial()
			
			base.material.bumpMap = normalBumpTex
			-----------------------------------
			
			bi.removeAllBakeElements()
			bi.bakeEnabled = false
			
			--get rid of projection mod
			deleteModifier base 1
			
			--hide low  density
			hide obj
			bSwitchLevel.caption = "Get Top Mesh"
		)
		
		on bCalcNormal pressed do
		(
			progressStart "Calculating normals..."
			suspendEditing()
			try
			(
				bakeNormal spnMapDim.value
			) catch
			(
				resumeEditing()
				progressEnd()
			)
			progressEnd()
			resumeEditing()
		)
		
		on bCalcDisp pressed do
		(
			with redraw off
			(
				progressStart "Calculating displacements..."
				suspendEditing()
				try
				(
					--backup & subdivide
					baseSubd = (copy base)
					hide base
						
					baseSubd.surfSubdivide = true
					baseSubd.subdivSmoothing = false
					baseSubd.isolineDisplay = false
					baseSubd.showCage = false
					baseSubd.iterations = spnDPSubPix.value
					convertToPoly baseSubd
						
					--render
					bakeDisp spnMapDim.value
				) catch
				(
					resumeEditing()
					progressEnd()
				)
				progressEnd()
				resumeEditing()
			)
		)
		
		on bDrop pressed do
		(
			if (yesNoCancelBox "Drop this model?") == #yes then
			(
				base = undefined
				obj = undefined
				detail = undefined
				
				toggleBaseDepButtons()
			)
		)
		
		on spnMapDim changed val do
		(
			spnMapDim.value = (nearestPowerOf2 val)
			--predictSubdivFaces()
		)
		
		on spnDPSubPix changed val do
		(
			--predictSubdivFaces()
		)
		
	)

	createDialog miniZB
)