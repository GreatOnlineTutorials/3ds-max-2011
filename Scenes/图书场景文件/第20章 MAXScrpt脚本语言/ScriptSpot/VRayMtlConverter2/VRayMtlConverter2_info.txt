The VRayMtl Converter v2.0, (C) by 3DZver - www.3dzver.ru (3dzver@bk.ru)

A simple MAXScript to automate otherwise time-consuming tasks regarding converting some types of materials (currently: Standard, Raytrace, Brazil Advanced and fr-Advanced) 
in VRay Materials (VRayMtl and VRyLightMtl), and also fixes some parameters of materials of previous versions V-Ray (1.09.xx) in view of changes in new versions V-Ray (1.46.xx).

This tool fine works with such types of materials as: Multimaterial, VRayMtlWrapper, Blend, Shellac, Composite.

-------

Instructions:

1: After installing run 3dsmax, in the Customize dialog go to the "3DZverTools" category.
2: Drag the "VRayMtlConverter v2.0" into any desired toolbar or put it in a quad menu.
3: Use this tool.

-------
This script is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this script.

------
WARNING!This script is free for any use except for profit by selling it.
If you want to distribute this script in any way, please contact
the author: 3dzver@bk.ru

--------