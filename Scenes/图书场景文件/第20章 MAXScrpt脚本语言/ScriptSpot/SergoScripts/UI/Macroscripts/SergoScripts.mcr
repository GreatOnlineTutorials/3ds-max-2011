-------------------------------------------------------------------------------
-- SergoScripts.mcr
-- MacroScripts File
-- By Sergo Pogosyan (sergo.pogosyan@gmail.com)
-- Created On: 10-02-2006
-------------------------------------------------------------------------------
macroScript BuildingMaker
	category:"Sergo Pogosyan" 
	toolTip:"Create Simple Buildings from the picked splines"
	buttonText:"Building Maker"
	Icon:#("sergoIcons",1)
(
	include "$scripts/SergoScripts/BuildingMaker.ms" 
)

macroScript StripeTexture
	category:"Sergo Pogosyan"
	toolTip:"Create bitmap with regular or random stripes"
	buttonText:"Stripe Texture"
	Icon:#("sergoIcons",2)
(
	include "$scripts/SergoScripts/StripeTexture.ms" 
)

macroScript WhiteVPBackground
	category:"Sergo Pogosyan"
	toolTip:"Make viewport background white"
	buttonText:"White Viewport"
	Icon:#("sergoIcons",3)
(
	include "$scripts/SergoScripts/WhiteVPBackground.ms"
)

macroScript TransformationJitter
	category:"Sergo Pogosyan"
	toolTip:"Apply random transformation to selected objects"
	buttonText:"Transformation Jitter"
	Icon:#("sergoIcons",4)
(
	include "$scripts/SergoScripts/TransformationJitter.ms"
)

macroScript RotateClockWise
	category:"Sergo Pogosyan"
	toolTip:"Rotate Object 90 degree on clockwise"
	buttonText:"Rotate 90 CW"
	Icon:#("sergoIcons",5)
(
	on isEnabled return
	(
		if (selection.count >= 1) and (subobjectlevel==0) then true	else false
	)
	
	on execute do
	(
		rot90cw = eulerangles 0 0 -90
		rotate $ rot90cw
	)
)

macroScript RotateCounterClockWise
	category:"Sergo Pogosyan"
	toolTip:"Rotate Object 90 degree on counterclockwise"
	buttonText:"Rotate 90 CCW"
	Icon:#("sergoIcons",6)
(
	on isEnabled return
	(
		if (selection.count >= 1) and (subobjectlevel==0) then true	else false
	)
	on execute do
	(
		rot90ccw = eulerangles 0 0 90
		rotate $ rot90ccw
	)
)

macroScript RotateAround
	category:"Sergo Pogosyan"
	toolTip:"Rotate Object 180 degree"
	buttonText:"Rotate 180"
	Icon:#("sergoIcons",7)
(
	on isEnabled return
	(
		if (selection.count >= 1) and (subobjectlevel==0) then true	else false
	)
	
	on execute do
	(
		rot90ccw = eulerangles 0 0 180
		rotate $ rot90ccw
	)
)