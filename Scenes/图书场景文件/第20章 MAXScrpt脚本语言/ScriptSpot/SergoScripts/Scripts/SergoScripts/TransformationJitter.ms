-------------------------------------------------------------------------------
-- TransformationJitter.ms
-- By Sergo Pogosyan (sergo.pogosyan@gmail.com)
-- v 1.0
-- Created On: 10-02-2006
-- tested using Max 8.0
-------------------------------------------------------------------------------
-- Description:
-- Apply random transformations to selected objects
-------------------------------------------------------------------------------
-- Usage:
-- Enter transformations and press Apply button
-- Reset button resets all spinners to 0
-------------------------------------------------------------------------------
-- Thanks to VG for adding scale axis locking
--


(
	global transJitter_floater

	rollout transJitter_mainRoll "Transformation Jitter"
	(
		group "Position"
			(
			radiobuttons coordspos labels:#("World", "Local")
			spinner spnXMinPos "X: Min:" range:[0,100000,0] across:2 fieldWidth:40 offset:[-2,0]
			spinner spnXMaxPos "Max:" range:[0,100000,0] fieldWidth:40
			spinner spnYMinPos "Y: Min:" range:[0,100000,0] across:2 fieldWidth:40 offset:[-2,0]
			spinner spnYMaxPos "Max:" range:[0,100000,0] fieldWidth:40
			spinner spnZMinPos "Z: Min:" range:[0,100000,0] across:2 fieldWidth:40 offset:[-2,0]
			spinner spnZMaxPos "Max:" range:[0,100000,0] fieldWidth:40
			)
			
		group "Rotation"
			(
			spinner spnXRot "X:" width:80 range:[0,359,0] offset:[7,0]
			spinner spnYRot "Y:" width:80 range:[0,359,0] offset:[7,0]
			spinner spnZRot "Z:" width:80 range:[0,359,0] offset:[7,0]
			)
	
		group "Scale"
			(
			checkBox uniform_scale "Uniform Scale" checked:false
			spinner spnXMinScale "X: Min:" range:[0,100000,0] across:2 fieldWidth:40 offset:[-2,0]
			spinner spnXMaxScale "Max:" range:[0,100000,0] fieldWidth:40
			spinner spnYMinScale "Y: Min:" range:[0,100000,0] across:2 fieldWidth:40 offset:[-2,0]
			spinner spnYMaxScale "Max:" range:[0,100000,0] fieldWidth:40
			spinner spnZMinScale "Z: Min:" range:[0,100000,0] across:2 fieldWidth:40 offset:[-2,0]
			spinner spnZMaxScale "Max:" range:[0,100000,0] fieldWidth:40
			)

			button btnApply "Apply" width:80
			button btnReset "Reset" width:80

		on uniform_scale changed state do
		(
			spnYMaxScale.enabled = not state
			spnZMaxScale.enabled = not state
			spnYMinScale.enabled = not state
			spnZMinScale.enabled = not state
		)

		on btnApply pressed do
		(
			undo "Transformation Jitter" on
			(
				for object in selection do
				(
					-- SCALING
					if uniform_scale.state then
					(
						uniScale = random -(spnXMinScale.value/100) (spnXMaxScale.value/100)
						object.scale.x = abs(object.scale.x + uniScale)
						object.scale.y = abs(object.scale.y + uniScale)
						object.scale.z = abs(object.scale.z + uniScale)
					)
					else
					(
						object.scale.x = abs(object.scale.x + random -(spnXMinScale.value/100) (spnXMaxScale.value/100))
						object.scale.y = abs(object.scale.y + random -(spnYMinScale.value/100) (spnYMaxScale.value/100))
						object.scale.z = abs(object.scale.z + random -(spnZMinScale.value/100) (spnZMaxScale.value/100))
					)
					-- SCALING

					-- POSITION
					if coordspos.state == 1 then
					in coordsys world
					(
						object.pos.x = object.pos.x + random -spnXMinPos.value spnXMaxPos.value
						object.pos.y = object.pos.y + random -spnYMinPos.value spnYMaxPos.value
						object.pos.z = object.pos.z + random -spnZMinPos.value spnZMaxPos.value
					)
					else
					in coordsys local
					(
						object.pos.x = (object.pos.x + random -spnXMinPos.value spnXMaxPos.value) - object.pos.x
						object.pos.y = (object.pos.y + random -spnYMinPos.value spnYMaxPos.value) - object.pos.y
						object.pos.z = (object.pos.z + random -spnZMinPos.value spnZMaxPos.value) - object.pos.z
					)
					-- POSITION

					-- ROTATION
					rotX = random -spnXRot.value spnXRot.value
					rotY = random -spnYRot.value spnYRot.value
					rotZ = random -spnZRot.value spnZRot.value

					rot = eulerangles rotX rotY rotZ

					rotate object rot
					-- ROTATION
				)
			)
		)

		on btnReset pressed do
		(
			spnXMinPos.value = spnXMaxPos.value = 0
			spnYMinPos.value = spnYMaxPos.value = 0
			spnZMinPos.value = spnZMaxPos.value = 0
			
			spnXRot.value = 0
			spnYRot.value = 0
			spnZRot.value = 0
			
			spnXMinScale.value = spnXMaxScale.value = 0
			spnYMinScale.value = spnYMaxScale.value = 0
			spnZMinScale.value = spnZMaxScale.value = 0
		)
	)

	rollout aboutRoll "About"
	(
		label lb_about "Transformation Jitter 1.0"
		label lb_author "Sergo Pogosyan"
		hyperLink lb_email "sergo.pogosyan@gmail.com" address:"mailto:sergo.pogosyan@gmail.com" align:#center
		label lb_year "2006"
	)

	if transJitter_floater != undefined do CloseRolloutFloater transJitter_floater
	transJitter_floater = newRolloutFloater "Transformation Jitter" 210 415
	addRollout transJitter_mainRoll transJitter_floater
	addRollout aboutRoll transJitter_floater
	aboutRoll.open = false
)