-------------------------------------------------------------------------------
-- prerender.ms
-- By Sergo Pogosyan (sergo.pogosyan@gmail.com)
-- v 1.0
-- Created On: 10-02-2006
-- tested using Max 8.0
-------------------------------------------------------------------------------
-- Description: 
-- prerender script for checking some vray renderer parameters after
-- rendering starts
-------------------------------------------------------------------------------
-- Usage:
-- select script in prerender script section of the render window
-- when rendering starts window will be opened with vray parameter
-------------------------------------------------------------------------------
global preRenderRollout
try(destroyDialog preRenderRollout)catch()

rollout preRenderRollout "prerender script"
(
	edittext outputText "Vray Settings" fieldWidth:324 height:200 labelOnTop:true
	on preRenderRollout open do
	(
		temptext = ""
		isVray = true
		
		VrayRenderer = renderers.current
		try(VrayRenderer.system_vrayLog_show == undefined)
		catch
		(
			tempText = "Vray renderer is not assigned"
			isVray = false
		)
		
		
		if isVray do
		(
			tempString = case VrayRenderer.imageSampler_type of
			(
				0:"Fixed Rate"
				1:"Adaptive QMC"
				2:"Adaptive Subdivision"
			)
			
			tempText = tempText + tempString + "\n"
			
			if VrayRenderer.gi_on then
			(
				tempString = "GI is on"
				tempText = tempText + tempString + "\n"
				
				tempString = case VrayRenderer.gi_primary_type of
				(
					0:"Irrandiance Map"
					1:"Photon Map"
					2:"Quasi-Monte Carlo"
					3:"Light Cache"
				)		
				tempText = tempText + "Primary Bounce: " + tempString + "\n"		
				tempString = case VrayRenderer.gi_secondary_type of
				(
					0:"None"
					1:"Photon Map"
					2:"Quasi-Monte Carlo"
					3:"Light Cache"
				)
				tempText = tempText + "Secondary Bounce: " + tempString + "\n"
			)
			else
			(
				tempString = "GI is off"
				tempText = tempText + tempString + "\n"
			)
		)	
		
			
		preRenderRollout.outputText.text = tempText
	)
)
	
createDialog preRenderRollout width:350 modal:false pos:[100,100]