-------------------------------------------------------------------------------
-- StripesTexture.ms
-- By Sergo Pogosyan (sergo.pogosyan@gmail.com)
-- v 1.0
-- Created On: 21-01-2006
-- tested using Max 8.0
-------------------------------------------------------------------------------
-- Description: 
-- Script for creating random or regular stripe texture
-- User has ability to adjust stripes' width, color and image resolution
-------------------------------------------------------------------------------
-- Usage:
-- Enter parameteres and press "Create Bitmap"
-------------------------------------------------------------------------------
-- TO DO list:
-- Major features
-- 1. assign random colors to stripes
-------------------------------------------------------------------------------


(
	global stripeTextureRollout
	global stripeTexture_floater

	global bitmapX = 512
	global bitmapY = 128
	
	stripeBitmap = bitmap bitmapX bitmapY color:[255,255,255]

	rcMenu stripeMenu
	(
		subMenu "File"
		(
			menuItem save_as "Save as..."
--			separator sep_01
--			menuItem exit_item "Exit"
		)

		on save_as picked do
		(
			theSaveName = getSaveFileName types:"BMP (*.bmp)|*.bmp|Targa (*.tga)|*.tga|JPEG (*.jpg)|*.jpg"
			if theSaveName != undefined do
			(
				stripeBitmap.filename = theSaveName
				save stripeBitmap
			)
		)
	)
	
	rollout tempRollOut "Bitmap"
	(
		bitmap theBitmap pos:[0,0] width:bitmapX height:bitmapY

		on tempRollOut open do
		(
			theBitmap.bitmap = stripeBitmap
		)
	)

	rollout stripeTextureRollout "Stripe Texture"
	(		
		group "Colors"
		(
			colorpicker color1 "Background Color" align:#right fieldwidth:60 color:[255,255,255] modal:false
			colorpicker color2 "Stripe Color" align:#right fieldwidth:60 modal:false
		)

		group "Bitmap Dimensions"
		(
			spinner bitmap_width "Bitmap Width: " range:[0,2048,512] type:#integer fieldWidth:40
			spinner bitmap_height "Bitmap Height: " range:[0,2048,128] type:#integer fieldWidth:40
		)

		group "Stripe Parameters"
		(
			spinner stripe_width "Stripe Width: From: " range:[0,2048,10] type:#integer across:2 fieldWidth:40 offset:[31,0]
			spinner stripe_width_max "To: " range:[0,2048,40] type:#integer across:1 fieldWidth:40 
			spinner gap_width "Gap Width: From: " range:[0,2048,5] type:#integer across:2 fieldWidth:40 offset:[31,0]
			spinner gap_width_max "To: " range:[0,2048,10] type:#integer across:1 fieldWidth:40
		)
		
		button butt01 "Open bitmap" width:80

		fn createStripeBitmap stripe_color gap_color =
		(
			--iter = abs (bitmapX/(stripe_width.value + gap_width.value))
			pixel_X = 0
			tempBitmap = bitmap bitmapX bitmapY color:gap_color

			while pixel_X < bitmapX do
			(
				tempWidth = random stripe_width.value stripe_width_max.value
				tempGapWidth = random gap_width.value gap_width_max.value
				for j=pixel_X to pixel_X + tempWidth do
				(
					for k=0 to bitmapY do
						setPixels tempBitmap [j, k] #(stripe_color)
				)
				pixel_X = pixel_X + tempWidth + tempGapWidth
			)
			stripeBitmap = tempBitmap
		)

		on butt01 pressed do
		(
			bitmapX = bitmap_width.value
			bitmapY = bitmap_height.value

			createStripeBitmap color2.color color1.color
			try(destroyDialog tempRollOut)catch()
			createDialog tempRollOut bitmapX bitmapY menu:stripeMenu
		)
	)
	
	rollout aboutRoll "About"
	(
		label lb_about "Stripe Texture 1.0"
		label lb_author "Sergo Pogosyan"
		hyperLink lb_email "sergo.pogosyan@gmail.com" address:"mailto:sergo.pogosyan@gmail.com" align:#center
		label lb_year "2006"
	)

	try(CloseRolloutFloater stripeTexture_floater)catch()
	--createDialog stripeTextureRollout width:250 style:#(#style_titlebar, #style_border, #style_minimizebox, #style_sysmenu)
	stripeTexture_floater = newRolloutFloater "Stripe Texture" 250 300
	addRollout stripeTextureRollout stripeTexture_floater
	addRollout aboutRoll stripeTexture_floater
	aboutRoll.open = false
)