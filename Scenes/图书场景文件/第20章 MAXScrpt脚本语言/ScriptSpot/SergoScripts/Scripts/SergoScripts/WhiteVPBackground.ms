-------------------------------------------------------------------------------
-- WhiteVPBackground.ms
-- By Sergo Pogosyan (sergo.pogosyan@gmail.com)
-- v 1.0
-- Created On: 10-02-2006
-- tested using Max 8.0
-------------------------------------------------------------------------------
-- Description:
-- On first click script changes viewport background color to white, on the
-- second returns color to the value of (125 125 125)
-------------------------------------------------------------------------------
-- Usage:
-- click the button to change color
-------------------------------------------------------------------------------
-- TO DO list:
-- Major features
-- 1. remember custom color and find a way to get viewport color properly
-- 		see commented lines - it doesn't work

(
--	global vp_standard_color = color 125 125 125
--	white_color = color 255 255 255
--	current_color = getVPortBGColor()
	global vp_color_changed
	if vp_color_changed == undefined do
	(
		vp_color_changed = false
	)
	
--	fn compare_colors color1 color2 =
--	(
--		white_color = color 255 255 255
--		result = false
--		comp_result = (white_color - color1) == (white_color - color2)
--		print color1
--		print color2
--		print comp_result
--		if  comp_result do result = true
--		result = result
--	)
	
--	if not compare_colors white_color current_color then 
--	(
--		print "remember stColor"
--		vp_standard_color = current_color
--		setVPortBGColor(color 255 255 255)
--		completeRedraw()
--	)
--	else
--	(	
--		print "restore stColor"
--		setVPortBGColor(vp_standard_color)
--		completeRedraw()
--	)

	if vp_color_changed then
	(
		setVPortBGColor(color 125 125 125)
		vp_color_changed = false
	)
	else
	(
		setVPortBGColor(color 255 255 255)
		vp_color_changed = true
	)
)