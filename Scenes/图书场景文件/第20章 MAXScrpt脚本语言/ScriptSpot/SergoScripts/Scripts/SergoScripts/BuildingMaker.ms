-------------------------------------------------------------------------------
-- BuildingMaker.ms
-- By Sergo Pogosyan (sergo.pogosyan@gmail.com)
-- v 1.0
-- Created On: 18-01-2006
-- tested using Max 8.0
-------------------------------------------------------------------------------
-- Description: 
-- Script for creating simple building models from splines.
-- Splines must be closed and can contain more than one spline.
-- Initial parameteres given on the assumption that your scene units are millimeters.
-- You can change them in the global variables section.
-------------------------------------------------------------------------------
-- Usage:
-- Type in paramateres in rollout and select one or more spline and click 'Use Selected' button
-- or pick spline by button 'Pick Spline'.
-- Each created model has property 'Number of Stories' in 'Properties'->'User Defined' tab
-------------------------------------------------------------------------------
-- TO DO list:
-- Major features
-- 1. DONE. add ability for changing heights of first and last floors separately from each other and from rest of the floors.
-- 2. add different kind of buildings e.g. building with separate floors and building with gap in place of floors.
-- 3. DONE. feature 3 must work for the first and last floor separately from rest of the floors.
-- 4. DONE. material IDs of different parts of building must be different.

-- Minor features
-- 1. DONE. Add user-defined property number of stories.
-- 2. DONE. Building object must have an appropriate name, e.g. "Building 01" BuildingMaker

(	
	global buildingMaker_build_floater
	global buildingMaker_mainRollOut

	global globals_undefined

	--trick for remembering last entered values in spinners
	if globals_undefined == undefined do
	(
		global buildingMaker_first_height = 3600
		global buildingMaker_typical_height = 3300
		global buildingMaker_last_height = 3000
		global buildingMaker_num = 10
		
		global buildingMaker_first_inset = 0
		global buildingMaker_last_inset = 0
		
		global buildingMaker_roof_enable = false
		global buildingMaker_roof_height = 300
		global buildingMaker_roof_inset = 1500
		
		global buildingMaker_smooth_enable = false
		global buildingMaker_first_matID = 1
		global buildingMaker_typical_matID = 2
		global buildingMaker_last_matID = 3
		global buildingMaker_concrete_matID = 4
		
		globals_undefined = true
	)
	
	fn shape_filt obj = isShapeObject obj
	
	fn create_building obj = 
	(
			addmodifier obj (normalModifier())
			--convertToMesh obj
			convertTo obj (Editable_Poly)
			faceSelection = #{1..(polyOp.getNumFaces obj)}
			polyOp.setFaceSelection obj faceSelection
			polyOp.setFaceMatID obj faceSelection buildingMaker_first_matID
			--extrude first storey
			if buildingMaker_first_inset != 0 do
			(	--inset first storey
				polyOp.bevelFaces obj faceSelection 0 -(buildingMaker_first_inset)
				allSelection = #{1..(polyOp.getNumFaces obj)}
				polyOp.deleteFaces obj (allSelection-faceSelection) delIsoVerts:true
			)
			polyOp.extrudeFaces obj faceSelection buildingMaker_first_height --extrude first storey
			polyOp.setFaceMatID obj faceSelection buildingMaker_concrete_matID
			if buildingMaker_num > 2 do --if number of stories more then 2 then create typical floors
			(
				if buildingMaker_first_inset != 0 do polyOp.bevelFaces obj faceSelection 0 buildingMaker_first_inset
				for i=1 to buildingMaker_num-2 do
				(
					polyOp.setFaceMatID obj faceSelection buildingMaker_typical_matID
					polyOp.extrudeFaces obj faceSelection buildingMaker_typical_height	--extrude typical stories
				)
			)
			if buildingMaker_num > 1 do --extrude last storey if number of stories more than 1
			(
				if buildingMaker_last_inset !=0 then
				(
					polyOp.setFaceMatID obj faceSelection buildingMaker_concrete_matID
					polyOp.bevelFaces obj faceSelection 0 -(buildingMaker_last_inset)	--inset last storey
					polyOp.retriangulate obj faceSelection	--retriangulate to avoid overlapped edges  
				)
				else
				(
					if buildingMaker_num == 2 then
					(
						polyOp.setFaceMatID obj faceSelection buildingMaker_concrete_matID
						polyOp.bevelFaces obj faceSelection 0 buildingMaker_first_inset	--inset last storey
						polyOp.retriangulate obj faceSelection	--retriangulate to avoid overlapped edges	
					)
				)
				polyOp.setFaceMatID obj faceSelection buildingMaker_last_matID
				polyOp.extrudeFaces obj faceSelection buildingMaker_last_height --extrude last storey
			)
			if buildingMaker_roof_enable do
			(
				polyOp.setFaceMatID obj faceSelection buildingMaker_concrete_matID
				polyOp.bevelFaces obj faceSelection 0 buildingMaker_roof_inset
				polyOp.extrudeFaces obj faceSelection buildingMaker_roof_height
			)
			
			if buildingMaker_smooth_enable do
			(
				--obj.autoSmoothThreshold = 45
				tempSelection = #{1..(polyOp.getNumFaces obj)}
				polyOp.setFaceSelection obj tempSelection
				polyOp.autoSmooth obj
				polyOp.setFaceSelection obj faceSelection
			)
			
			convertToMesh obj
			update obj
			obj.name = uniqueName "Building "
			setUserProp obj "Number of Stories" buildingMaker_num
	)
	
	

	fn selectionHasShape sel =
	(
		result = false
		for i=1 to sel.count do
		(
			obj = sel[i]
			if shape_filt obj do result = true
		)
		result = result
	)
	
	fn shapeIsClosed shapeObj shapeName =
	(
		convertToSplineShape shapeObj
		
		result = true
		--nameString = format "%n" shapeObj.name
		--nameString = "Spline " + nameString + "is open"
		for i=1 to numSplines shapeObj do
		(
			if not isClosed shapeObj i do
			(
				result = false
				messageString = "Spline "+ shapeName +" is open" 
				messageBox messageString title:"Close all splines!"
				exit
			)
		)
		if not result do delete shapeObj
		res = result
	)
	
	rollout buildingMaker_mainRollOut "Building Maker"
	(
		fn enableButton =
		(
			if (selection.count >= 1) and (selectionHasShape selection) then 
				buildingMaker_mainRollOut.useSelected.enabled = true else 
					buildingMaker_mainRollOut.useSelected.enabled = false
		)
		
		pickbutton chooseit "Pick Spline" width:140 filter:shape_filt
		button useSelected "Use Selected" width:140 enabled:((selection.count >= 1) and (selectionHasShape selection)) --use this button to create building using already selected shape

		on chooseit picked obj do
		(
			if obj != undefined do
			(
				shape_obj = copy obj
								
				if shapeIsClosed shape_obj obj.name do
					undo "Create Building" on create_building shape_obj
			)
		)

		on useSelected pressed do
		(
			if selection != undefined do
			(
				undo "Create Building" on
				(
					for i=1 to selection.count do
					(
						obj = selection[i]
						if shape_filt obj do
						(
							shape_obj = copy obj
							if shapeIsClosed shape_obj obj.name do create_building shape_obj
						)
					)
				)
			)
		)
		
		group "Dimensions"
		(
			spinner first_storey_height "First Storey:" range:[0,10000,buildingMaker_first_height] type:#float
			spinner typical_storey_height "Typical Storey: " range:[0,10000,buildingMaker_typical_height] type:#float
			spinner last_storey_height "Last Storey:" range:[0,10000,buildingMaker_last_height] type:#float
			spinner storey_num "Num of Stories: " range:[1,1000,buildingMaker_num] type:#integer
		)
		on first_storey_height changed val do buildingMaker_first_height = val
		on typical_storey_height changed val do buildingMaker_typical_height = val
		on last_storey_height changed val do buildingMaker_last_height = val
		on storey_num changed val do buildingMaker_num = val
		
		group "Insets"
		(
			spinner first_storey_inset "First Storey Inset:" range:[0,10000,buildingMaker_first_inset] type:#float
			spinner last_storey_inset "Last Storey Inset:" range:[0,10000,buildingMaker_last_inset] type:#float
		)
		on first_storey_inset changed val do buildingMaker_first_inset = val
		on last_storey_inset changed val do buildingMaker_last_inset = val

		group "Roof"
		(
			checkBox roof_enable "Create Roof" checked:buildingMaker_roof_enable
			spinner roof_height "Roof height:" range:[0,10000,buildingMaker_roof_height] enabled:buildingMaker_roof_enable type:#float
			spinner roof_inset "Roof Overhang:" range:[0,10000,buildingMaker_roof_inset] enabled:buildingMaker_roof_enable type:#float
		)
		on roof_enable changed val do
		(
			buildingMaker_roof_enable = roof_enable.state
			roof_height.enabled = roof_enable.state
			roof_inset.enabled = roof_enable.state
		)
		on roof_height changed val do buildingMaker_roof_height = val
		on roof_inset changed val do buildingMaker_roof_inset = val
		
		group "Surface Parameters"
		(
			checkBox smooth_enable "Smooth Result" checked:buildingMaker_smooth_enable
			spinner first_matID "First Storey MatID:" range:[0,65535,buildingMaker_first_matID] fieldWidth:50 align:#right
			spinner typical_matID "Typical Storey MatID:" range:[0,65535,buildingMaker_typical_matID] fieldWidth:50 align:#right
			spinner last_matID "Last Storey MatID:" range:[0,65535,buildingMaker_last_matID] fieldWidth:50 align:#right
			spinner concrete_matID "Concrete MatID:" range:[0,65535,buildingMaker_concrete_matID] fieldWidth:50 align:#right
		)
		on smooth_enable changed val do buildingMaker_smooth_enable = smooth_enable.state
		on first_matID changed val do buildingMaker_first_matID = val
		on typical_matID changed val do buildingMaker_typical_matID = val
		on last_matID changed val do buildingMaker_last_matID = val
		on concrete_matID changed val do buildingMaker_concrete_matID = val


		on buildingMaker_mainRollOut close do callbacks.removeScripts #selectionSetChanged		
	)

	rollout aboutRoll "About"
	(
		label lb_about "Building Maker 1.0"
		label lb_author "Sergo Pogosyan"
		hyperLink lb_email "sergo.pogosyan@gmail.com" address:"mailto:sergo.pogosyan@gmail.com" align:#center
		label lb_year "2006"
	)
	
	callbacks.addScript #selectionSetChanged "buildingMaker_mainRollOut.enableButton()"
	
	if buildingMaker_build_floater != undefined do CloseRolloutFloater buildingMaker_build_floater
	buildingMaker_build_floater = newRolloutFloater "Building Maker" 200 505
	addRollout buildingMaker_mainRollOut buildingMaker_build_floater
	addRollout aboutRoll buildingMaker_build_floater
	aboutRoll.open = false	
)