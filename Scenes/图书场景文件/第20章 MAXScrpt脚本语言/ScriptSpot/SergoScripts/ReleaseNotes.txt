several scripts by Sergo Pogosyan
e-mail: sergo.pogosyan@gmail.com

release date: 14-03-2006

USAGE: unpack into 3ds max ROOT directory, restart max, then go to the 'Customize User Interface' -> 'Toolbars', browse to "Sergo Pogosyan" category and add buttons to the toolbar.


SCRIPTS IN PACK:

BuildingMaker		- Script for creating simple building models from splines
StripesTexture		- Script for creating random or regular stripe texture
TransformationJitter	- Apply random transformations to selected objects
WhiteVPBackground 	- Toggle change viewport background color to white or to the default (125,125,125)
				to change default VP color to your settings, edit the script
Prerender		- script for checking some vray renderer parameters after rendering starts
RotateClockWise, 
RotateCounterClockWise, 
RotateAround 		- three scripts for easy, one-click rotating objects on 90, -90 and 180 degree
