--
--   Meshtools
--   by Laszlo Sebo (lsebo@matavnet.hu)
--
--
--   Compiled Version
--   V2.5
--
--   All macroscripts now in one file, for easier file management.
--
----------------------------------------------------------------------------------------------------


macroScript Connect_1
	category:"Meshtools"
	toolTip:"Connect Selected Edges/Vertices/Polys"
	Icon:#("Meshtools",1)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  ((classof $.baseobject == Editable_Poly) and ((subobjectlevel ==4) or (subobjectlevel ==2) or (subobjectlevel==1)))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  ((classof $.baseobject == Editable_Poly) and ((subobjectlevel ==4) or (subobjectlevel ==2) or (subobjectlevel==1)))then true else false
			else false
	)
	on execute do
	(
		if (subobjectlevel == 1) then
		(
			Meshtools_Connect_Vertex $.baseobject (polyop.getvertselection $.baseobject)
			update $
		) else
		if (subobjectlevel == 2) then
		(
			Meshtools_Connect_Edge $.baseobject 1
			update $
		) else
		(
			if (Meshtools_Connect_Poly $.baseobject) == false do (max undo)
			update $
		)
	)
)

macroScript Connect_2
	category:"Meshtools"
	toolTip:"Connect Selected Edges - 2"
	Icon:#("Meshtools",2)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  ((classof $.baseobject == Editable_Poly) and (subobjectlevel ==2))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  ((classof $.baseobject == Editable_Poly) and (subobjectlevel ==2))then true else false
			else false
	)
	on execute do
	(
		Meshtools_Connect_Edge $.baseobject 2
		update $
	)
)

macroScript Connect_3
	category:"Meshtools"
	toolTip:"Connect Selected Edges - 3"
	Icon:#("Meshtools",3)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  ((classof $.baseobject == Editable_Poly) and (subobjectlevel ==2))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  ((classof $.baseobject == Editable_Poly) and (subobjectlevel ==2))then true else false
			else false
	)
	on execute do
	(
		Meshtools_Connect_Edge $.baseobject 3
		update $
	)
)

macroScript DivideEdge_2
	category:"Meshtools"
	toolTip:"Divide Selected Edges to 2"
	Icon:#("Meshtools",7)
(
	on isEnabled return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==2)) then 
			if  (classof $.baseobject == Editable_Poly)then true else false
			else false
		
	)

	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==2)) then 
			if  (classof $.baseobject == Editable_Poly)then true else false
			else false
		
	)
	
	on Execute do
	(
		local OldNumVerts = polyop.getnumverts $.baseobject
		Meshtools_Divide_Edges $.baseobject (polyop.getedgeselection $.baseobject) 2
		local Selectthese = #()
		for i = (OldNumVerts + 1) to (polyop.getnumverts $.baseobject) do append Selectthese i
		polyop.setvertselection $.baseobject Selectthese
		subobjectlevel = 1
		update $
	)

)

macroScript DivideEdge_3
	category:"Meshtools"
	toolTip:"Divide Selected Edges to 3"
	Icon:#("Meshtools",8)
(
	on isEnabled return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==2)) then 
			if  (classof $.baseobject == Editable_Poly)then true else false
			else false
		
	)

	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==2)) then 
			if  (classof $.baseobject == Editable_Poly)then true else false
			else false
		
	)
	
	on Execute do
	(
		local OldNumVerts = polyop.getnumverts $.baseobject
		Meshtools_Divide_Edges $.baseobject (polyop.getedgeselection $.baseobject) 3
		local Selectthese = #()
		for i = (OldNumVerts + 1) to (polyop.getnumverts $.baseobject) do append Selectthese i
		polyop.setvertselection $.baseobject Selectthese
		subobjectlevel = 1
		update $
	)

)

macroScript DivideEdge_4
	category:"Meshtools"
	toolTip:"Divide Selected Edges to 4"
	Icon:#("Meshtools",9)
(
	on isEnabled return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==2)) then 
			if  (classof $.baseobject == Editable_Poly)then true else false
			else false
		
	)

	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==2)) then 
			if  (classof $.baseobject == Editable_Poly)then true else false
			else false
		
	)
	
	on Execute do
	(
		local OldNumVerts = polyop.getnumverts $.baseobject
		Meshtools_Divide_Edges $.baseobject (polyop.getedgeselection $.baseobject) 4
		local Selectthese = #()
		for i = (OldNumVerts + 1) to (polyop.getnumverts $.baseobject) do append Selectthese i
		polyop.setvertselection $.baseobject Selectthese
		subobjectlevel = 1
		update $
	)
)


-- Known Issues:
-- I had to remove ' and ($.selectededges.count==1)' from the Editable Mesh if statement,
-- because the isEnabled property does not get refreshed with selection changes...
--

macroScript EdgeLoop
	category:"Meshtools"
	toolTip:"Edge Loop Select"
	Icon:#("Meshtools",18)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==2)) then 
			if (((classof $ == Editable_Mesh)) or
			 (classof $.baseobject == Editable_Poly)) then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==2)) then 
			if (((classof $ == Editable_Mesh)) or
			 (classof $.baseobject == Editable_Poly)) then true else false
			else false
	)

	on execute do
	(
		if (classof $.baseobject == Editable_Poly) then
		(
			polyop.setedgeselection $ (Meshtools_EdgeLoop $.baseobject (polyop.getedgeselection $.baseobject))
			$.baseobject.setFaceFlags #{} 1  -- set the 'dirty' flag to force an update (gizmo&selection))
		)	else
		(
			local selectedEdges = for i in $.selectededges collect i.index
			local newEdgeSelection = Meshtools_EdgeLoop $ selectedEdges
			if newEdgeSelection == false do newEdgeSelection = selectedEdges
			select $.edges[newEdgeSelection]
			update $
		)
	)
)

macroScript EdgeRing
	category:"Meshtools"
	toolTip:"Edge Ring Select"
	Icon:#("Meshtools",19)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==2)) then 
			if  (classof $.baseobject == Editable_Poly) then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==2)) then 
			if  (classof $.baseobject == Editable_Poly) then true else false
			else false
	)

	on execute do
	(
		polyop.setedgeselection $.baseobject (Meshtools_EdgeRing $.baseobject (polyop.getedgeselection $.baseobject))
		$.baseobject.setFaceFlags #{} 1
	)
)

macroScript EraseVertexClean
	category:"Meshtools"
	toolTip:"Erase Vertex Clean method"
	Icon:#("Meshtools",10)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==1)) then 
			if (classof $.baseobject == Editable_Poly) then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==1)) then 
			if (classof $.baseobject == Editable_Poly) then true else false
			else false
	)

	on execute do
	(
		try
		(
			if (Meshtools_Erase_Vertex $.baseobject 1 (polyop.getvertselection $.baseobject)) == false do
			messagebox "DANG! I suggest an UNDO now... The eraser has probably made an error,\nlike deleting various edges/vertices in your object.\nDon't use it on OPEN vertices!\nYet..."
		)
		catch
		(
			enablesceneredraw()
			messagebox "DANG! I suggest an UNDO now... The eraser has made an error.\nPlease send an example of the situation to Laszlo Sebo (lsebo@matavnet.hu)\n"
		)
		update $
	)
)

macroScript EraseVertexDirt
	category:"Meshtools"
	toolTip:"Erase Vertex Dirty method"
	Icon:#("Meshtools",11)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==1)) then 
			if (classof $.baseobject == Editable_Poly) then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel==1)) then 
			if (classof $.baseobject == Editable_Poly) then true else false
			else false
	)

	on execute do
	(
		try
		(
			if (Meshtools_Erase_Vertex $.baseobject 2 (polyop.getvertselection $.baseobject)) == false do
			messagebox "DANG! I suggest an UNDO now... The eraser has probably made an error,\nlike deleting various edges/vertices in your object.\nDon't use it on OPEN vertices!\nYet..."
		)
		catch
		(
			enablesceneredraw()
			messagebox "DANG! I suggest an UNDO now... The eraser has made an error.\nPlease send an example of the situation to Laszlo Sebo (lsebo@matavnet.hu)\n"
		)
		update $
	)
)


macroScript FlattenX
	category:"Meshtools"
	toolTip:"Flatten Selected verts along X axis"
	Icon:#("Meshtools",12)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  ((classof $.baseobject == Editable_Poly) and (subobjectlevel ==1))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  ((classof $.baseobject == Editable_Poly) and (subobjectlevel ==1))then true else false
			else false
	)
	on execute do
	(
		Meshtools_FlattenX $.baseobject
		update $
	)
)

macroScript FlattenY
	category:"Meshtools"
	toolTip:"Flatten Selected verts along Y axis"
	Icon:#("Meshtools",13)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  ((classof $.baseobject == Editable_Poly) and (subobjectlevel ==1))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  ((classof $.baseobject == Editable_Poly) and (subobjectlevel ==1))then true else false
			else false
	)
	on execute do
	(
		Meshtools_FlattenY $.baseobject
		update $
	)
)


macroScript FlattenZ
	category:"Meshtools"
	toolTip:"Flatten Selected verts along Z axis"
	Icon:#("Meshtools",14)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  ((classof $.baseobject == Editable_Poly) and (subobjectlevel ==1))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  ((classof $.baseobject == Editable_Poly) and (subobjectlevel ==1))then true else false
			else false
	)
	on execute do
	(
		Meshtools_FlattenZ $.baseobject
		update $
	)
)


macroScript Grow
	category:"Meshtools"
	toolTip:"Grow Selection"
	Icon:#("Meshtools",23)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined)) then 
			if  (((subobjectlevel!=0) and (subobjectlevel!=undefined))) and ((classof $.baseobject == Editable_Poly) or (classof $ == Editable_Mesh))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0) and (subobjectlevel!=undefined))) then 
			if  ((classof $.baseobject == Editable_Poly) or (classof $ == Editable_Mesh))then true else false
			else false
	)

	on execute do
	(
		if (classof $ == Editable_Mesh) then (
			Meshtools_Grow $ 
			update $
		) else (
			Meshtools_Grow $.baseobject
			$.baseobject.setFaceFlags #{} 1 
		)
	)
)

macroScript Shrink
	category:"Meshtools"
	toolTip:"Shrink Selection"
	Icon:#("Meshtools",24)

(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0) and (subobjectlevel!=undefined))) then 
			if  ((classof $.baseobject == Editable_Poly) or (classof $ == Editable_Mesh))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0) and (subobjectlevel!=undefined))) then 
			if  ((classof $.baseobject == Editable_Poly) or (classof $ == Editable_Mesh))then true else false
			else false
	)

	on execute do
	(
		if (classof $ == Editable_Mesh) then (
			Meshtools_Shrink $ 
			update $
		) else (
			Meshtools_Shrink $.baseobject
			$.baseobject.setFaceFlags #{} 1
		)
	)
)

macroScript Inset
	category:"Meshtools"
	toolTip:"Inset selected polygons"
	Icon:#("Meshtools",15)
(
	on isEnabled return
	(
		if ((selection.count == 1) and (classof selection[1].baseobject == Editable_Poly) and (subobjectlevel == 4)) then true
		else false
	)

	on isVisible return
	(
		if ((selection.count == 1) and (classof selection[1].baseobject == Editable_Poly) and (subobjectlevel == 4)) then true
		else false
	)
	
	on Execute do
	(
		if ((bitarray2array (polyop.getfaceselection $.baseobject)).count > 0) do 
		(
			pushprompt "Press and Hold Left Mouse Button to start Inset..."
			try (starttool Meshtools_Inset) catch (stoptool Meshtools_Inset)
		)
	)

)

macroScript Outline
	category:"Meshtools"
	toolTip:"Outline"
	Icon:#("Meshtools",17)

(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel!=undefined)) then 
			if  (((classof $.baseobject == Editable_Poly) and (subobjectlevel == 4)) or 
			( (classof $ == Editable_Mesh) and ( (subobjectlevel == 3) or (subobjectlevel == 4) ) ) )
			then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and (subobjectlevel!=undefined)) then 
			if  (((classof $.baseobject == Editable_Poly) and (subobjectlevel == 4)) or 
			( (classof $ == Editable_Mesh) and ( (subobjectlevel == 3) or (subobjectlevel == 4) ) ) )
			then true else false
			else false
	)

	on execute do
	(
		if (classof $ == Editable_Mesh) then (
			Meshtools_Outline $ 
			update $
		) else (
			Meshtools_Outline $.baseobject 
			$.baseobject.setFaceFlags #{} 1
		)
	)
)


macroScript SelectVert
	category:"Meshtools"
	toolTip:"Convert selection to vertex level"
	Icon:#("Meshtools",6)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0)and (subobjectlevel!=undefined) )) then 
			if  ((classof $.baseobject == Editable_Poly) or (classof $ == Editable_Mesh))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0)and (subobjectlevel!=undefined) )) then 
			if  ((classof $.baseobject == Editable_Poly) or (classof $ == Editable_Mesh))then true else false
			else false
	)

	on execute do
	(
		if (classof $ == Editable_Mesh) then Meshtools_ConvertToVertex $ else Meshtools_ConvertToVertex $.baseobject 
		update $
	)
)


macroScript SelectEdge
	category:"Meshtools"
	toolTip:"Convert selection to edge level"
	Icon:#("Meshtools",4)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0)and (subobjectlevel!=undefined) )) then 
			if  ((classof $.baseobject == Editable_Poly) or (classof $ == Editable_Mesh))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0)and (subobjectlevel!=undefined) )) then 
			if  ((classof $.baseobject == Editable_Poly) or (classof $ == Editable_Mesh))then true else false
			else false
	)

	on execute do
	(
		if (classof $ == Editable_Mesh) then Meshtools_ConvertToEdge $ else Meshtools_ConvertToEdge $.baseobject
		update $
	)
)

macroScript SelectPoly
	category:"Meshtools"
	toolTip:"Convert selection to polygon level"
	Icon:#("Meshtools",5)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0)and (subobjectlevel!=undefined) )) then 
			if  ((classof $.baseobject == Editable_Poly) or (classof $ == Editable_Mesh))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0)and (subobjectlevel!=undefined) )) then 
			if  ((classof $.baseobject == Editable_Poly) or (classof $ == Editable_Mesh))then true else false
			else false
	)

	on execute do
	(
		if (classof $ == Editable_Mesh) then Meshtools_ConvertToPolygon $ else Meshtools_ConvertToPolygon $.baseobject 
		update $
	)
)

macroScript SharedVert
	category:"Meshtools"
	toolTip:"Select shared vertices of current SOs"
	Icon:#("Meshtools",22)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0)and (subobjectlevel!=1) and (subobjectlevel!=undefined) )) then 
			if  ((classof $.baseobject == Editable_Poly))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0)and (subobjectlevel!=1) and (subobjectlevel!=undefined) )) then 
			if  ((classof $.baseobject == Editable_Poly))then true else false
			else false
	)

	on execute do
	(
		Meshtools_SharedVertex $.baseobject 
		update $
	)
)

macroScript SharedEdge
	category:"Meshtools"
	toolTip:"Select shared edges of current SOs"
	Icon:#("Meshtools",20)
(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0)and (subobjectlevel!=2)and (subobjectlevel!=3)and (subobjectlevel!=undefined) )) then 
			if  ((classof $.baseobject == Editable_Poly))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0)and (subobjectlevel!=2)and (subobjectlevel!=3)and (subobjectlevel!=undefined) )) then 
			if  ((classof $.baseobject == Editable_Poly))then true else false
			else false
	)

	on execute do
	(
		Meshtools_SharedEdge $.baseobject 
		update $
	)
)


macroScript SharedPolygon
	category:"Meshtools"
	toolTip:"Select shared polygons of current SOs"
	Icon:#("Meshtools",21)

(
	on isEnabled return 
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0)and (subobjectlevel!=4) and (subobjectlevel!=undefined) )) then 
			if  ((classof $.baseobject == Editable_Poly))then true else false
			else false
	)
	on isVisible return
	(
		if (((getcurrentselection()).count == 1) and ($ != undefined) and ((subobjectlevel!=0)and (subobjectlevel!=4) and (subobjectlevel!=undefined) )) then 
			if  ((classof $.baseobject == Editable_Poly))then true else false
			else false
	)

	on execute do
	(
		Meshtools_SharedPolygon $.baseobject 
		update $
	)
)

macroScript Tighten
	category:"Meshtools"
	toolTip:"Tighten Selected Vertices"
	Icon:#("Meshtools",25)
(
	on isEnabled return
	(
		if ((selection.count == 1) and (classof selection[1].baseobject == Editable_Poly) and (subobjectlevel == 1)) then true
		else false
	)

	on isVisible return
	(
		if ((selection.count == 1) and (classof selection[1].baseobject == Editable_Poly) and (subobjectlevel == 1)) then true
		else false
	)
	
	on Execute do
	(
		if ((bitarray2array (polyop.getvertselection $.baseobject)).count > 0) do 
		(
			pushprompt "Press and Hold Left Mouse Button to start Tighten..."
			try (starttool Meshtools_Tighten) catch (stoptool Meshtools_Tighten)
		)
	)

)


macroscript SurfaceSelect
	category:"Meshtools"
	tooltip:"Select material ID surface"
	Icon:#("Meshtools",31)
(
	on isEnabled return
	(
		if ((selection.count == 1) and (classof selection[1].baseobject == Editable_Poly) and (subobjectlevel == 4)) then true
		else false
	)

	on isVisible return
	(
		if ((selection.count == 1) and (classof selection[1].baseobject == Editable_Poly) and (subobjectlevel == 4)) then true
		else false
	)
	
	on Execute do
	(
		local newSelection = Meshtools_SurfaceSelect $.baseobject
		polyop.setfaceselection $.baseobject newSelection
		$.baseobject.setFaceFlags #{} 1 
	)
	
)

macroscript SpinEdge
	category:"Meshtools"
	tooltip:"Spin selected edge in poly"
	Icon:#("Meshtools",28)

(
	on isEnabled return
	(
		if ((selection.count == 1) and (classof selection[1].baseobject == Editable_Poly) and (subobjectlevel == 2)) then true
		else false
	)

	on isVisible return
	(
		if ((selection.count == 1) and (classof selection[1].baseobject == Editable_Poly) and (subobjectlevel == 2)) then true
		else false
	)
	
	on Execute do
	(
		local edge_s = bitarray2array (polyop.getedgeselection $.baseobject)
		if edge_s.count == 1 then
			if (Meshtools_SpinEdge $.baseobject edge_s[1]) then (redrawviews())		
	)
)

macroscript Solid_Chamfer
	category:"Meshtools"
	tooltip:"Chamfer, but keeps the original edges"
	Icon:#("Meshtools",29)
(
	on isEnabled return
	(
		if ((selection.count == 1) and (classof selection[1].baseobject == Editable_Poly) and ((subobjectlevel == 2) or (subobjectlevel == 3))) then true
		else false
	)

	on isVisible return
	(
		if ((selection.count == 1) and (classof selection[1].baseobject == Editable_Poly) and ((subobjectlevel == 2) or (subobjectlevel == 3))) then true
		else false
	)
	
	on Execute do
	(
		if ((bitarray2array (polyop.getedgeselection $.baseobject)).count > 0) do 
		(
			pushprompt "Press and Hold Left Mouse Button to start Solid-Chamfer..."
			try (starttool Meshtools_SolidChamfer) catch (stoptool Meshtools_SolidChamfer;enablesceneredraw())
		)
	)
)

macroscript Ignore_backfacing_toogle
	category:"Meshtools"
	tooltip:"Ignore backfacing toogle"
	Icon:#("Meshtools",26)
(
	if (selection.count == 1) and (classof $.baseobject == Editable_Poly)
		then $.baseobject.ignorebackfacing = not $.baseobject.ignorebackfacing
)

macroscript Soft_selection_toogle
	category:"Meshtools"
	tooltip:"Soft Selection toogle"
	Icon:#("Meshtools",30)
(
	if (selection.count == 1) and (classof $.baseobject == Editable_Poly)
		then $.baseobject.useSoftSel = not $.baseobject.useSoftSel
)

macroscript ShowEndResult_toogle
	category:"Meshtools"
	tooltip:"Show End Result toogle"
	Icon:#("Meshtools",27)
(
	showEndresult = not ShowEndResult
)

macroscript MultiBevel
	category:"Meshtools"
	tooltip:"Multiple Iteration Bevel - experimental stuff"
	Icon:#("Meshtools",16)
(


	
	if (selection.count == 1) and (classof $.baseobject == Editable_Poly) and (subobjectlevel == 4) and ((polyop.getfaceselection $.baseobject as array).count == 1) then
	(
		try (starttool Meshtools_MultiBevel_Mousetool)
		catch (
			enablesceneredraw()
			enablesceneredraw()
		)	
	)
	unregisterRedrawViewsCallback Meshtools_StateShow
	redrawviews()



)