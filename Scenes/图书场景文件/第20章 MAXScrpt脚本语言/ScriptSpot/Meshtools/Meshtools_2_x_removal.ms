-- Meshtools_Installer
-- by Laszlo Sebo 2001
-- Run me just once please.
-- Don't modify. If you do, everything will go bad. Believe me.

-- deletion of old meshtools macroscripts:
deleteFile "$macroscripts\Meshtools-Connect_1.mcr"
deleteFile "$macroscripts\Meshtools-Connect_2.mcr"
deleteFile "$macroscripts\Meshtools-Connect_3.mcr"
deleteFile "$macroscripts\Meshtools-DivideEdge_2.mcr"
deleteFile "$macroscripts\Meshtools-DivideEdge_3.mcr"
deleteFile "$macroscripts\Meshtools-DivideEdge_4.mcr"
deleteFile "$macroscripts\Meshtools-EdgeLoop.mcr"
deleteFile "$macroscripts\Meshtools-EdgeRing.mcr"
deleteFile "$macroscripts\Meshtools-EraseVertexClean.mcr"
deleteFile "$macroscripts\Meshtools-EraseVertexDirt.mcr"
deleteFile "$macroscripts\Meshtools-FlattenX.mcr"
deleteFile "$macroscripts\Meshtools-FlattenY.mcr"
deleteFile "$macroscripts\Meshtools-FlattenZ.mcr"
deleteFile "$macroscripts\Meshtools-Grow.mcr"
deleteFile "$macroscripts\Meshtools-Inset.mcr"
deleteFile "$macroscripts\Meshtools-Outline.mcr"
deleteFile "$macroscripts\Meshtools-SelectEdge.mcr"
deleteFile "$macroscripts\Meshtools-SelectPoly.mcr"
deleteFile "$macroscripts\Meshtools-SelectVert.mcr"
deleteFile "$macroscripts\Meshtools-SharedEdge.mcr"
deleteFile "$macroscripts\Meshtools-SharedPolygon.mcr"
deleteFile "$macroscripts\Meshtools-SharedVert.mcr"
deleteFile "$macroscripts\Meshtools-Shrink.mcr"
deleteFile "$macroscripts\Meshtools-Tighten.mcr"
deleteFile "$macroscripts\Meshtools-SpinEdge.mcr"

















