The new tools:
    -Soft selection toogle
    -Ignore backfacing toogle
    -Show end result toogle
    -Surface Select (selects polys, connected to current poly selection with matching matIDs)
    -SpinEdge (like edge turn in emesh... but look out, with funky topology, the turning edge can run 'away')
    -SolidChamfer (like the normal edge chamfer, but this keeps the original edges also. Look out, it doesn't make good mapping coords like normal chamfer. maybe in a future version)
    -MultiBevel (make a bevel operation on ONE poly, rotate it along x-y-z, then repeat this automatically (interactively). funny tool, try it ;)

INSTALLATION (READ IT!):

1. run "Meshtools_2_x_removal.ms". This will (should) delete all previous
   version Meshtools macroscripts from 3dsmax4/ui/macroscripts directory. (I
   compiled now all scripts into 2 files, so they are easier to manage.)
2. copy "Meshtools-Functions.ms" to 3dsmax4/stdplugs/stdscripts (where
   3dsmax4 is your max4 directory), overwriting the previous file
3. copy "Meshtools-Macroscripts.mcr" to 3dsmax4/ui/macroscripts
4. copy the new icon files (all .bmp s) to ui/icons and ui/discreet.
5. pray
6. Start max.
7. go to Customize/CustomizeUI
8. Everything should be under Category:Meshtools, as before.