E-Light Installation
------------------------------------
E-Light works in 3D Studi MAX 3, 4 and 5.
Copy E-Light.ms into any directory, for example C:\3dsmax4\scripts.
In 3D Studio MAX, choose MAXScript, Run Script..., and select E-Light.ms.
Minimise the E-Light floater when you're not using it.
Or once you've closed it, run it again. It will automatically read your scene and detect all object settings.
Alternatively, create a shortcut for it if you plan to use it a lot.

There is a seperate version for MAX 5, due to interface lay-out changes in this version of MAX.

E-Light should be pretty self-explanatory in its use.
If there is a need for a tutorial with tips and tricks, please mail me and I'll try and write one! :)

I hope you'll enjoy this script! Please mail me with your feedback!

Ronnie Olsthoorn
------------------------------------
skyraider3d@hotmail.com
http://www.skyraider3d.com


PS. Special thanks go to Alexander Esppeschit Bicalho and Borislav Petrov for their excellent tutorials on MAXScripting!!!