Installation notes for Game Level Builder 2.22 max3 edition by Michael Little.

--  Note: You must have these MaxScript Extensions installed or this script will not work!
--  Make sure max is not running before you follow these instructions.
--   1:MAXScript Control Library 2.2, CtrlLib.dlx for max 3
--   2:Binary File Stream v1.0, BinStream.dlx for max 3
--   3:Avguard Extensions - Version 2.09, avg_dlx.dlx
--  You can download this from www.scriptspot.com's complete list of extensions
Check that these 3 .dlx files are directly in the plugins folder and not in any subfolders before running max 3.

If you have GLB2.21 installed you should uninstall it by deleting the .ms files from the GLB2 folder and the
GLB2_21max3.mcr file from the UI\macroscripts folder in your 3dsmax3_1 folder.

To install GLB2.22
Copy the GLB2 folder from the max3 folder in this zip file to your scripts folder.
Copy the two .bmp files(splines_24a and splines_24i) in this folder to the GLB2 folder in your scripts folder.
Copy the Sample Files to the GLB2 folder (optional).

You can then copy the GLB2_22max3.mcr file to your
UI\macroscripts folder.
If you wish you can add a button on your user Interface to run GLB
by going into your Customize menu and choosing the
'Customize UI...' option, then choose 'Macro Scripts' and from the category drop down list
select 'Maple3D'. Then add the buttons to a toolbar.
Note:You can also create a GLB2 Tab panel by right clicking on the Tab Panel and choosing 'Add Tab...'

Run GLBuilderV2_22max3.ms (which should now be in the GLB2 subfolder
in your scripts folder), through max's MAXScript menu.

For Help read the 'GLB22 Help.doc' file or go to www.maple3d.com/forum .