macroScript GLB2 
category:"Maple3D" 
tooltip:"GLB2.22" 
buttonText:"GLB 2.22" 
(on execute do fileIn "$scripts\\GLB2\\GLBuilderV2_22max4-8.ms")

macroScript HandyCam 
category:"Maple3D" 
tooltip:"Handy Cam 1.12" 
buttonText:"HandyCam 1.12" 
(on execute do fileIn "$scripts\\GLB2\\HandyCamV1_12.ms")

macroScript PipeMax 
category:"Maple3D" 
tooltip:"PipeMax 1.02" 
buttonText:"PipeMax 1.02" 
(on execute do fileIn "$scripts\\GLB2\\PipeMaxV1_02.ms")

macroScript MapExporterLE 
category:"Maple3D" 
tooltip:"Map Exporter LE" 
buttonText:"Map Exporter LE" 
(on execute do fileIn "$scripts\\GLB2\\MapExporterV1LE.mse")