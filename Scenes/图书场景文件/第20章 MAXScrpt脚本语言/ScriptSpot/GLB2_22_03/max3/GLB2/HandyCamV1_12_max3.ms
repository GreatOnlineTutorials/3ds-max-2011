-- HandyCamV1_0_max3.ms
--
-- Title:Handy Cam 1.0 max3 edition
-- Created: Oct 22 2001
-- Last Updated:Aug 3 2002
--
-- Author : Michael Little, maple3d@hotmail.com
-- For: 3ds max 3.x
-- Optimized for UI res of 1280,960
-- For GameLevelBuilder 2.0 max 3 edition
-- *************************
-- Requires: MAXScript Control Library 2.2, CtrlLib.dlx for max 3, By Simon Feltman, simon@asylum-soft.com
-- Download from http://www.asylum-soft.com or www.scriptspot.com's extensions section
-- **************************
-- 
-- Discription: A WorldCraft style 1st person camera mover. Creates splines that follow the path you travel in 1st person mode.
--
-- New Features in Version 1.1,
--		Added 'Altitude Lock' which makes camera always stay at the same height unless trucked using the alt key. (Thanks to Moog for suggestion).
--		Minimum and maximum speed have been increased so camera can now go slower and faster than before.
--		Added ForwardDueToMouse variable which should help stop the camera from shooting faster when mouse is moved if tweaked (look at Options below).
--
-- How to:
-- The 'dis' checkbox disables other views while F.P. Cam is active; which can speed up the cameras motion (recommended).
-- Spd Spinner sets the speed
-- Acl Spinner sets the acceleration
-- When active(Start Cam pressed): Left-Click toggles camera hardwire(lock) to mouse movement.
-- When active(Start Cam pressed): Ctrl is forward, Shift is Backward,Ctrl + Shift is slow forward, Alt is Truck Camera, Ctrl + Alt is level camera while moving.
-- Ctrl + Shift + Alt is slow forward and level
-- The 'Rec' checkbox enables the recording of the cameras movement which creates a spline line for the path that the camera traveled.
-- The space spinner controls the spacing at which points are made, smaller numbers mean more points.
-- The 'Altitude Lock' checkbox makes the camera stay at its current height off the grid. To increase or decrease the altitude of
-- the camera while in this mode hold down the alt key (only), and move the mouse forward or back.
-- Right-Click to stop.

-- Note: I found that Handy-Cam did not work as well when using the Elsa Maxtreme drivers due to the viewports not getting disabled properly.
--		   Use the OpenGL drivers if possible.
--
-- Using Altitude Lock while recording a path can give bad results, use the alt key to lock the camera level instead while recording.
-- If using a wireless keyboard, make sure it is sending a strong signal or strange things may happen.

-- 1.12
-- View renames to HandyCam.
--***********************************************************************************************
-- MODIFY THIS AT YOUR OWN RISK

clearlistener()
try(destroyDialog FPCamRollout);catch()

rollout FPCamRollout "Handy Cam V1.12"

(
checkbutton StartCam "Start Cam" width:70 pos:[2,2] height:16 tooltip:"Activates Cam, Left-Click to toggle camera panning to mouse movement"
checkbox vwdisable "Dis" pos:[78,3] checked:true
spinner spdSpn "Spd:" pos:[0,22] range:[0.1,99,4] fieldwidth:26 scale:1
spinner accSpn "Acl:" pos:[59,22] range:[0.01,5,0.2] fieldwidth:26
checkbox recpath "Rec" pos:[2,41]
spinner Spcspn "Space" pos:[46,42] range:[2,90,15] fieldwidth:26 type:#integer enabled:false
checkbox AltLockBox "Altitude Lock" pos:[2,59]
timer clock "Loopy" interval:20 active:false

----- Options: (YOU CAN CHANGE THESE VARIABLES WITHOUT MUCH RISK :) --
Local RotSensitivity = 0.6 -- Panning speed with mouse, where 0.0 means no movement and 2.0 means high sensitivity
Local NoFlipping = True -- Stops camera from flipping upside-down. Set to False to allow flipping (inverted camera).
Local FldView = 90 -- Field of view angle for created camera.
Local Clipdist = 2000 -- Far clipping distance for created camera.
Local ForwardDueToMouse = 0.65 -- Tweak between 0.1 and 1 if camera appears to jump forward or slow down when mouse is moved. Default 0.65.
-----------------------
-----------------------
Local TopSpeed = 4 -- if you change this then change the 3rd value in the range of spinner spdSpn above to be the same.
Local acc = 0.2 -- if you change this then change the 3rd value in the range of spinner accSpn above to be the same.
Local MinSpeed = 0.1
Local DeskSize = sysInfo.DesktopSize
local deskCenter = DeskSize*0.5
Local camtransmtx
Local bub,sh = 0
Local FPCam = undefined
Local AmbCol
Local CamInView = undefined
Local AngleDiv = 15 -- Changed by space spinner, a smaller number means less space between points, thus increasing accuracy.
Local tk2
Local LastPt
Local c = undefined
Local centered = false
Local Altitude = 64.0

on spdSpn changed val do TopSpeed = val
on accSpn changed val do acc = val
on Spcspn changed val do AngleDiv = val
on recpath changed state do if state then Spcspn.enabled = true else Spcspn.enabled = false

fn StartSpline = (
		c = splineshape()
		addnewspline c
		addknot c 1 #corner #curve FPCam.pos
		LastPt = FPCam.pos
		bub = FPCam.pos
)

fn StopSpline = (
		addknot c 1 #corner #curve FPCam.pos;
		bub = (getOutVec c 1 ((numKnots c 1) - 1) - (getKnotPoint c 1 ((numKnots c 1) - 1)))
		setOutVec c 1 ((numKnots c 1) - 1) ((getKnotPoint c 1 ((numKnots c 1) - 1)) - bub*((distance (LastPt) FPCam.pos)/3))
		c.pivot = c.center
		local Dname = dummy pos:[0,0,0] prefix:"Camera Path "
		c.name = Dname.name
		delete Dname
		c.rotation = quat 0 0 0 1
		c.scale = [1,1,1]
		c.renderable = true	-- Sets line to be renderable in General properties
		c.thickness = 2		-- Sets thickness in General properties
		Updateshape c
		if (numKnots c 1 == 2 and (getKnotPoint c 1 1 == getKnotPoint c 1 2)) do delete c
		)

fn RotationFilter L = ( -- Stops camera from flipping upside-down
if L < 0 do (if abs(L) > 90 then L = 180 else L = 0)
Return L
)

tool FstPersonCam (
	local screendist = [0,0]
	local oldposx
	local oldposy
	Local MoveOk = false
		
	on mousePoint pointno do (
		if pointno == 1 then ((oldposx = getcursorpos();oldposx = oldposx.x;oldposy = getcursorpos();oldposy = oldposy.y))
			else (if MoveOk then MoveOk = False else MoveOk = true)
		if pointno > 1 do (if CamInView != undefined then (if viewport.numviews >= CamInView do viewport.activeviewport = CamInView) else viewport.activeviewport = viewport.numviews)
	)
	
	on mousemove clickno do
	(
		Local sh2 = (TopSpeed*ForwardDueToMouse),sh3 = (TopSpeed*0.15)
		Local newposx = getcursorpos();newposx = newposx.x
		Local newposy = getcursorpos();newposy = newposy.y
		Local Invec,OutVec,dist,pkt
		Local v = (getcursorpos() - DeskCenter),Dist = (length v)
		camtransmtx = FPCam.objecttransform
		if centered == true do (oldposx = newposx; oldposy = newposy; centered = false)
		screendist = [newposx - oldposx,newposy - oldposy]
		if MoveOk do (
				if recpath.checked do (
					tk2 += 1
					if keyboard.ControlPressed do (if (mod tk2 AngleDiv) == 0 do (
						dist = ((distance (LastPt) FPCam.pos)/3)
						in Coordsys camtransmtx (InVec = [0,0,dist]; OutVec = [0,0,1]
						addknot c 1 #beziercorner #curve [0,0,0] InVec OutVec);LastPt = FPCam.pos
						Updateshape c; 
						pkt = (getKnotPoint c 1 ((numKnots c 1) - 1))
						bub = (getOutVec c 1 ((numKnots c 1) - 1) - pkt)
						setOutVec c 1 ((numKnots c 1) - 1) (pkt - bub*dist) )))
				if abs(v.x) > deskSize.x*0.46 do if FPCam != undefined do(setcursorpos ([deskcenter.x,(v + deskcenter).y]);centered = true) -- Sets cursor back to center when at screen edges.
				if (Ctrlkey and Altkey) or (Shiftkey and Altkey) or (not Altkey) do (FPCam.rotation.z_rotation -= (screendist.x*RotSensitivity))
				if (not AltKey) do (FPCam.rotation.x_rotation -= (screendist.y*RotSensitivity);if NoFlipping do (FPCam.rotation.x_rotation = RotationFilter (FPCam.rotation.x_rotation)))
				if (Ctrlkey and Altkey) or (Shiftkey and Altkey) do (FPCam.rotation.x_rotation = 90 )
				if CtrlKey and not Shiftkey do (in Coordsys camtransmtx (move FPCam [0,0,(-1)*sh2]);if AltLockBox.checked do FPCam.pos.z = Altitude)
				if Shiftkey and not Ctrlkey do (in Coordsys camtransmtx (move FPCam [0,0,sh2]);if AltLockBox.checked do FPCam.pos.z = Altitude)
				if (Altkey and not Ctrlkey and not Shiftkey) do (in Coordsys camtransmtx (move FPCam [(screendist.x*2),(-2)*(screendist.y),0]);if AltLockBox.checked do Altitude = FPCam.pos.z)
				if (Ctrlkey and Shiftkey) do (if sh < sh3 do (sh += acc; sh3 = sh);in Coordsys camtransmtx (move FPCam [0,0,(-1)*sh3]);if AltLockBox.checked do FPCam.pos.z = Altitude)
			)
		oldposx = newposx
		oldposy = newposy
		)
		
	on mouseAbort abortno do if abortno > 0 do (StartCam.state = off;clock.active = false;if recpath.checked do (StopSpline();print "Recording stopped");ambientColor = AmbCol;if vwdisable.checked do (for i = 1 to viewport.numviews do (viewport.activeviewport = i;if viewport.getcamera() != FPCam do max vpt disable));#stop)
)

on StartCam changed state do (if state == on then
	(CamInView = undefined
	FPCam = undefined
	if viewport.numviews == 0 do (print "No Views Found"; return ok)
	AmbCol = ambientColor
	ambientColor = color 140 140 140
	if cameras != undefined do (for i = 1 to cameras.count do if cameras[i].name == "HandyCam" do FPCam = cameras[i])
	if FPCam == undefined do FPCam = (freecamera rotation:(angleaxis 90 [1,0,0]) pos:[0,0,64] fov:FldView clipManually:on nearclip:3 farclip:Clipdist name:"HandyCam")
	if AltLockBox.checked do Altitude = (FPCam.pos.z);
	for i = 1 to viewport.numviews do (viewport.activeviewport = i;if viewport.getcamera() == FPCam then CamInView = i else if vwdisable.checked do max vpt disable)
	if CamInView != undefined then (viewport.activeviewport = CamInView) else (viewport.activeviewport = viewport.numviews;viewport.setcamera FPCam;max vpt disable)
	sh = MinSpeed
	camtransmtx = FPCam.objecttransform
	clock.active = true;if recpath.checked do (tk = 0;tk2 = 0;StartSpline();Print "Recording started"); startTool FstPersonCam) else (clock.active = false;if recpath.checked do (StopSpline();print "Recording stopped"); stopTool FstPersonCam))

on clock tick do (Local v = (getcursorpos() - DeskCenter),Dist = (length v)
	if (keyboard.ControlPressed and not keyboard.shiftPressed) do (if sh < TopSpeed do sh += acc;in Coordsys camtransmtx (move FPCam [0,0,(-1)*sh]);if AltLockBox.checked do FPCam.pos.z = Altitude)
	if (not keyboard.ControlPressed and not keyboard.shiftPressed) do (if sh > MinSpeed do (sh -= 1;if sh < MinSpeed do sh = MinSpeed))
	if (keyboard.shiftPressed and not keyboard.ControlPressed) do (camtransmtx = FPCam.objecttransform;if sh < TopSpeed do sh += acc;in Coordsys camtransmtx (move FPCam [0,0,sh]);if AltLockBox.checked do FPCam.pos.z = Altitude)
	if (keyboard.ControlPressed and keyboard.shiftPressed) do (camtransmtx = FPCam.objecttransform;in Coordsys camtransmtx (move FPCam [0,0,(-1)*(TopSpeed*0.25)]);if AltLockBox.checked do FPCam.pos.z = Altitude)
	if abs(v.x) > deskSize.x*0.46 do if FPCam != undefined do(setcursorpos ([deskcenter.x,(v + deskcenter).y]);centered = true) -- Sets cursor back to center when at screen edges.
	)
	
on FPCamRollout open do (clock.active = false)
on FPCamRollout close do (StopTool FstPersonCam)
	
)
global diagpos = [425,100]
if (sysinfo.desktopsize).x == 1600 do diagpos = [736,98]
if (sysinfo.desktopsize).x == 1400 do diagpos = [544,98]
if (sysinfo.desktopsize).x == 1360 do diagpos = [504,98]
if (sysinfo.desktopsize).x == 1024 do diagpos = [302,0]
if (sysinfo.desktopsize).x < 1024 do diagpos = [0,0]
CreateDialog FPCamRollout Width:125 height:65 Pos:diagpos style:#style_toolwindow


-- End of Script