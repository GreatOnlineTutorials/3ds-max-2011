-- PipeMaxV1_02max3.ms
-- Created by Michael and John Little
-- Copyright �2002, Michael Little, www.maple3d.com, maple3d@hotmail.com
-- last update:Nov 12 2003
-- for Max 3.x
-- Used by Game Level Builder 2.2 max3 edition.

-- Discription: Create Pipes, Ducts, Tunnels, Arches and much more, by simply drawing lines.
--				This tool works similar to lofting in max.
--				Very good help when level building for Half-Life or Quake.

--			Notes:
--				You can create solid rectangular pipes by feeding in a single line for the Shape when not in quick mode.
--				The Shape is always centered on the Path line at its pivot point. You can move the pivot point using the
--				hierarchy panel to get different results.
--				If you use a small ngon as the path, this tool works very like the lathe modifier.
--				The Polygons number on the UI states the number of quadrilateral polygons as for WorldCraft, not the usual triangular
--				polygon count that max displays with its 'Polygon Counter'.

-- Updates:July 2002. Added mapping for pipes.
-- Fixed bug where pipes that started at a slope had shrunken ends.
-- Updated:Sept 15 2003. Pipe orientation made more user friendly.
-- If Selection lock was on pipes would not get grouped properly. Fixed!
-------------------------------------
gc()
clearlistener()

try(destroyDialog PipeMaxDialog);catch()

rollout PipeMaxDialog " PipeMax v1.021 max3"
(

	fn SplineFilter obj = superclassof obj == Shape

	button DLine "Line" pos:[10,5] width:30 height:30 images:#((GetDir #Scripts) + "\GLB2\Splines_24i.bmp", (GetDir #Scripts) + "\GLB2\Splines_24a.bmp", 11, 1, 1, 1, 1) toolTip:"Line, for drawing Pipes. Draw in Top View."
	pickbutton pLine "Pick Path" pos:[48,5] width:90 height:30 filter:SplineFilter tooltip:"Press this then pick the line which the pipe will follow."
	pickbutton pShp "Pick Shape" pos:[145,5] width:130 height:18 filter:SplineFilter tooltip:"Press this then pick the shape of the pipe."
	spinner DiamSpn "Pipe Diameter:" pos:[-200,5] width:112 fieldwidth:46 height:16 range:[2,2048,48] scale:8
	spinner ThickSpn "Pipe Thickness:" pos:[165,27] width:112 fieldwidth:46 height:16 range:[1,1024,8] type:#float scale:1
	spinner SideSpn "Sides:" pos:[290,5] width:64 fieldwidth:30 height:16 range:[3,128,4] type:#integer scale:1 enabled:false
	spinner AngleSpn "Angle:" pos:[369,5] width:60 fieldwidth:30 height:16 range:[0,360,0] type:#integer scale:1
	checkbox Easymd "Quick Mode" pos:[440,28] width:80 height:15
	button CrtPipe "Create Pipe" pos:[440,5] width:100 height:20 enabled:false tooltip:"Press to create pipe. Path must be picked."
	label polylab "Polygons:" pos:[295,28]
	edittext lab "" pos:[342,26] fieldwidth:80 text:"0"

local easymode = false
local KnotPoint = #(),knotTypes = #(),InVec = #(),OutVec = #()
local pipegroup = #()
fn GetShape shp = (
		ConvertToSplineShape shp;
		for h = 1 to (numSplines shp) do (KnotPoint = #();knotTypes = #();InVec = #();OutVec = #()
			for k = 1 to (numknots shp h) do (append KnotPoint (getKnotPoint shp h k);append knotTypes (getKnotType shp h k);
			append InVec (GetInVec shp h k);append OutVec (GetOutVec shp h k))
			)
)
local rad = (48/cos(180.0/4))/2.0 -- same as (DiamSpn.value/cos(180.0/SideSpn.value))/2.0
local theta,thick,nps = 4.0
local z = #()

fn poly e vlong z t =(
	local r,phi
	r = length(z)
	phi = acos(z.x/r)
	if(z.y < 0.0)do(phi = 360 - phi)
	return(r*(cos(phi + t)*vlong + sin(phi + t)*e))
)	

fn rotq v q =((q * (quat v.x v.y v.z 0.0) * conjugate(q)).axis)

fn pos_rot v1 v2 v =(	-- v1, v2 are non-zero vectors orthogonal to the vector v, whose direction gives a sense of rotation
local t					-- to the normal plane. Returns the angle of rotation, modulo 360 degrees, from v1 to v2.
	t = acos(dot(normalize v1)(normalize v2))
	if((dot v (cross v1 v2)) < 0)do(t = 360 - t)
	return t
)	

local DB,clsd

fn outTube wp ep vlongp w e vlong = (
local i,j,k,m
local v,e1,e2,e3,len,b,pp,theta2
local f,fp,g,za = #(),zat = #(),zb = #(),zbt = #()
	v = w - wp
	e1 = normalize(v)
	theta2 = theta - (pos_rot ep e e1)
	len = length(v)
	if(clsd)then(m = nps)else(m = nps - 3)
	for u = 1 to (m + 1) do(
		j = u
		if(j > nps)do(j -= nps)
		i = u - 1
		if(i < 1)do(i += nps)
		k = u + 1
		if(k > nps)do(k -= nps)	
		fp = cross (normalize(z[j] - z[i])) [0,0,1]
		f = cross (normalize(z[k] - z[j])) [0,0,1]
		if(length(fp + f) > .000001)then(g = normalize(fp + f))else(g = f)
		g = (thick*g/(dot g f))
		append za (poly ep vlongp (z[j] - g) theta)
		append zat (poly ep vlongp (z[j] + g) theta)
		append zb (poly e vlong (z[j] - g)theta2)
		append zbt (poly e vlong (z[j] + g) theta2))
	for u = 1 to m do(
		i = u
		j = u + 1
		if(j > nps)do(j -= nps)
		pp = (za[i] + za[j] + zb[i] + zb[j])/4.0
			e2 = normalize(pp)
			e3 = cross e1 e2
			pos4 = 0.5*(wp + w) + pp
			
		STrans = (matrix3 e1 e2 e3 pos4)
		
		in coordsys STrans(
		B = box width:len height:thick length:20 lengthsegs:1 widthsegs:1 heightsegs:1 rotation:(quat 0 0 0 1) pos:[0,0,0] prefix:(DB.name + " section"))
		convertToMesh b
		append pipegroup b
		setvert b (1) (wp + za[j])
		setvert b (3) (wp + za[i])
		setvert b (5) (wp + zat[j])
		setvert b (7) (wp + zat[i])
		setvert b (4) (w + zb[i])
		setvert b (2) (w + zb[j])
		setvert b (6) (w + zbt[j])
		setvert b (8) (w + zbt[i])
		addModifier B (uvwmap ());B.modifiers[#UVW_Mapping].maptype = 4;B.modifiers[#UVW_Mapping].height = 256;B.modifiers[#UVW_Mapping].width = 256;B.modifiers[#UVW_Mapping].length = 256
	)
	theta = theta2
)

local Cmode

fn Curvey_Pipes inp = (
local knpt,ph
local clsdpath
setWaitCursor();if getCommandPanelTaskmode() == #modify then (max create mode;CMode = true) else CMode = false;
clearselection()
setUserProp pLine.object "Pipe-Line" 1
local dbname
thick = (ThickSpn.value)/2.0
z = #()
theta = (AngleSpn.value as float)
clsdpath = (isClosed pLine.object 1)
if easymode then (
-- easymode
	if not clsdpath do theta -= 90
	nps = SideSpn.value;clsd = true
	dbname = ((nps as integer)as string + " Sided Pipe ")
	(DB = Dummy boxsize:[25,25,25] rotation:(quat 0 0 0 1) pos:pLine.object.pos prefix:dbname)
	for i = 1 to nps do(
		ph = i*360.0/(nps)
		if mod nps 2 == 0 do ph += 180.0/(nps)
		append z ([rad*sin(ph),rad*cos(ph),0]) ))
else(
-- not easymode
	theta -= 90
	if clsdpath do theta -= 90
	if isDeleted pShp.object do (messagebox "Shape line has been deleted.\nPlease pick a new shape.";CrtPipe.enabled = false;return ok)
	setUserProp pShp.object "Pipe-Line" 1
	getshape pShp.object
	nps = Knotpoint.count
	clsd = isClosed pShp.object 1
	local sidenum
	if not clsd then sidenum = nps - 1 else sidenum = nps
	dbname = ((sidenum as integer)as string + " Sided Custom Pipe ")
	(DB = Dummy boxsize:[25,25,25] rotation:(quat 0 0 0 1) pos:pLine.object.pos prefix:dbname)
	if not(clsd)do(
		append KnotPoint (100*normalize(KnotPoint[nps] - KnotPoint[nps - 1]) + KnotPoint[nps])
		append KnotPoint (100*normalize(KnotPoint[1] - KnotPoint[2]) + KnotPoint[1])
		nps += 2)
	for i = 1 to nps do(
		knpt = knotpoint[i] - pShp.object.pos
		if clsdpath then append z ([knpt.x,-knpt.y,0]) else append z ([-knpt.x,knpt.y,0]) ))
------------
getshape inp		-- the cross section
	local wp,w,wn,e1,e2,e1p,e2p,vlong,vlongp
	local i,n,f,m
	n = KnotPoint.count
	if not(clsdpath)do(
		append KnotPoint (100*normalize(KnotPoint[n] - KnotPoint[n - 1]) + KnotPoint[n])
		append KnotPoint (100*normalize(KnotPoint[1] - KnotPoint[2]) + KnotPoint[1])
		n += 2)
		
	w = KnotPoint[n]
	if(clsdpath)then(m = n + 2)else(m = n - 1)
	
	for u = 1 to m do(
		i = 1 + mod (u - 1) n
		wn = KnotPoint[i]
		e1 = normalize(wn - w)
		if(u == 1)then( -- 1st pipe piece
			if(abs(dot e1 [0,0,1]) > .999)then(e1p = e1;e2p = normalize(cross e1 [0,1,0]))
			else(e1p = [0,0,1]))
		f = e1 - e1p
		if(length f > .000001)then(
			f = normalize f
			vlong = f/(dot f (normalize(cross(cross e1 f) e1))))
		else(vlong = normalize(cross e1 e2p)) 
		e2 = normalize(cross e1 vlong)
		if(u > 2)do(outTube wp e2p vlongp w e2 vlong)
		
		e1p = e1
		wp = w
		w = wn
		e2p = e2
		vlongp = vlong
		)
	delete DB;group pipegroup prefix:dbname select:on;if CMode do max modify mode;setArrowCursor()
)

	local labval = 0
	local linesegs = 1
	on DLine pressed do	(StartObjectCreation Line)
	on pLine picked obj do (ConvertToSplineShape obj; linesegs = (numsegments obj 1);
			if (Easymd.checked) or (pShp.object != undefined and not isDeleted pShp.object) do (CrtPipe.enabled = true; if (Easymd.checked) then (labval = ((nps*6*linesegs)as integer) as string; lab.text = labval) else (labval = (6*linesegs*(numsegments pShp.object 1))as string; lab.text = labval)))
	on pShp picked obj do (ConvertToSplineShape obj;if (pLine.object != undefined) do (CrtPipe.enabled = true;labval = (6*(numsegments obj 1)*linesegs)as string; lab.text = labval)	)
	on ThickSpn changed val do (if ThickSpn.value > DiamSpn.value do (DiamSpn.value = ThickSpn.value + 0.001; rad = (DiamSpn.value/cos(180.0/SideSpn.value))/2.0);thick = (ThickSpn.value)/2.0)
	on SideSpn changed val do (nps = SideSpn.value; rad = (DiamSpn.value/cos(180.0/SideSpn.value))/2.0;if pLine.object != undefined then (labval = (nps*6*linesegs)as string; lab.text = labval) else (labval = (nps*6)as string; lab.text = labval))
	on DiamSpn changed val do (if ThickSpn.value > DiamSpn.value do ThickSpn.value = DiamSpn.value - 0.001; rad = (DiamSpn.value/cos(180.0/SideSpn.value))/2.0)
	on AngleSpn changed val do theta = (AngleSpn.value as float)
	
	on Easymd changed state do	(if state == on then 
		(easymode = true;DiamSpn.pos = [260,5];pShp.pos = [-200,5];SideSpn.enabled = true
		if pLine.object != undefined then (CrtPipe.enabled = true;labval = ((nps*6*linesegs)as integer) as string;lab.text = labval) else CrtPipe.enabled = false)
			else (easymode = false;DiamSpn.pos = [-200,5];pShp.pos = [145,5];SideSpn.enabled = false
			if (pShp.object != undefined and pLine.object != undefined) then (CrtPipe.enabled = true;labval = (6*(numsegments pShp.object 1)*linesegs)as string;lab.text = labval) else CrtPipe.enabled = false))
			
	on CrtPipe pressed do (if not isDeleted pLine.object then Curvey_Pipes pLine.object
		else (messagebox "Path line has been deleted.\nPlease pick a new path.";CrtPipe.enabled = false))
)

fn GetPosForRes = --Gets pos for window according to resolution.
(
	local wpos = [550,470]
	case (sysinfo.desktopsize).x of
	(
	1280: ()
	1600: wpos = [860,470]
	1400: wpos = [665,470]
	1024: wpos = [420,380]
	1152: wpos = [420,470]
	1360: wpos = [625,470]
	default: wpos = [(sysinfo.desktopsize).x - 555,380]
	)
wpos
)
(
local WSize = [550,37]
CreateDialog PipeMaxDialog width:WSize.x height:WSize.y pos:(GetPosForRes()) style:#(#style_sysmenu, #style_toolwindow)
)


-- End of Script