-- UI buttons for GLB2.22 max3 edition
macroScript GLB2 
category:"Maple3D" 
tooltip:"GLB2" 
buttonText:"GLB2.22" 
(fileIn ((GetDir #Scripts) + "\GLB2\GLBuilderV2_22max3.ms"))

macroScript HandyCam 
category:"Maple3D" 
tooltip:"Handy Cam 1.12" 
buttonText:"Handy Cam" 
(fileIn ((GetDir #Scripts) + "\GLB2\HandyCamV1_12_max3.ms"))

macroScript PipeMax 
category:"Maple3D" 
tooltip:"PipeMax" 
buttonText:"PipeMax 1.02" 
(fileIn ((GetDir #Scripts) + "\GLB2\PipeMaxV1_02max3.ms"))

macroScript MapExporterLE 
category:"Maple3D" 
tooltip:"Export To Map" 
buttonText:"Map Exporter" 
(fileIn ((GetDir #Scripts) + "\GLB2\MapExporterV1LEmax3.mse"))