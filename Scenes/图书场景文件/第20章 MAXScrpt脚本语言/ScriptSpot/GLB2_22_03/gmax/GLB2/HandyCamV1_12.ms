-- HandyCamV1_12.ms
--
-- Title:Handy Cam Version 1.12
-- Created: Oct 22 2001
-- Last Updated:Dec 4 2003
--
-- Author :  Michael Little, info@maple3d.com, www.maple3d.com
-- For:  max 4, 5, 6 and gmax v1.x
-- Optimized for UI res of 1280,960
-- For Game Level Builder V2.2
-- 
-- Discription: A WorldCraft style 1st person camera mover. Creates splines that follow the path you travel in 1st person mode.
--
-- New Features in Version 1.1,
--		Added 'Altitude Lock' which makes camera always stay at the same height unless trucked using the alt key. (Thanks to Moog for suggestion).
--		Minimum and maximum speed have been increased so camera can now go slower and faster than before.
--		Added ForwardDueToMouse variable which should help stop the camera from shooting faster when mouse is moved if tweaked (look at Options below).
--
-- New to Version 1.12
--		Added Ui adjustment for 3ds max 5
--		Renamed in view to Handy Cam from First Person Cam

-- Updated Dec 4 03 for resolution of 1400x1050
--
-- How to:
-- The 'dis' checkbox disables other views while F.P. Cam is active; which can speed up the cameras motion (recommended).
-- Spd Spinner sets the speed
-- Acl Spinner sets the acceleration
-- When active(Start Cam pressed): Left-Click toggles camera hardwire(lock) to mouse movement.
-- When active(Start Cam pressed): Ctrl is forward, Shift is Backward,Ctrl + Shift is slow forward, Alt is Truck Camera, Ctrl + Alt is level camera while moving.
-- Ctrl + Shift + Alt is slow forward and level
-- The 'Rec' checkbox enables the recording of the cameras movement which creates a spline line for the path that the camera traveled.
-- The space spinner controls the spacing at which points are made, smaller numbers mean more points.
-- The 'Altitude Lock' checkbox makes the camera stay at its current height off the grid. To increase or decrease the altitude of
-- the camera while in this mode hold down the alt key (only), and move the mouse forward or back.
-- Right-Click to stop.

-- Note: I found that Handy-Cam did not work as well when using the Elsa Maxtreme drivers due to the viewports not getting disabled properly.
--		   Use the OpenGL drivers if possible.
--
-- Using 'Altitude Lock' while recording a path can give bad results, try using the alt and ctrl key instead.
-- If using a wireless keyboard, make sure it is sending a strong signal or strange things may happen.
--
-- modified to work with max 6.

--***********************************************************************************************
-- MODIFY THIS AT YOUR OWN RISK

clearlistener()
try(destroyDialog FPCamRollout);catch()

rollout FPCamRollout "Handy Cam V1.12" width:115 height:66

(
checkbutton StartCam "Start Cam" width:70 pos:[2,2] height:16 tooltip:"Activates Cam, Left-Click to toggle camera panning to mouse movement"
checkbox vwdisable "Dis" pos:[78,3] checked:true
spinner spdSpn "Spd:" pos:[0,22] range:[0.1,99,4] fieldwidth:24 scale:1
spinner accSpn "Acl:" pos:[59,22] range:[0.01,5,0.2] fieldwidth:24
checkbox recpath "Rec" pos:[2,41]
spinner Spcspn "Space" pos:[46,42] range:[2,90,15] fieldwidth:24 type:#integer enabled:false
checkbox AltLockBox "Altitude Lock" pos:[2,59]
timer clock "Loopy" interval:20 active:false

----- Options: (YOU CAN CHANGE THESE VARIABLES WITHOUT MUCH RISK :) --
Local RotSensitivity = 0.6 -- Panning speed with mouse, where 0.0 means no movement and 2.0 means high sensitivity
Local NoFlipping = True -- Stops camera from flipping upside-down. Set to False to allow flipping (inverted camera).
Local TopRot = 5 -- Controls speed which camera turns at when near the screen edges.
Local FldView = 90 -- Field of view angle for created camera.
Local Clipdist = 2000 -- Far clipping distance for created camera.
Local ForwardDueToMouse = 0.65 -- Tweak between 0.1 and 1 if camera appears to jump forward or slow down when mouse is moved. Default 0.65.
-----------------------
-----------------------
Local TopSpeed = 4 -- if you change this then change the 3rd value in the range of spinner spdSpn above to be the same.
Local acc = 0.2 -- if you change this then change the 3rd value in the range of spinner accSpn above to be the same.
Local MinSpeed = 0.1
Local DeskSize = sysInfo.DesktopSize
local deskCenter = DeskSize*0.5
Local camtransmtx
Local bub,sh
Local FPCam = undefined
Local AmbCol
Local CamInView = undefined
Local AngleDiv = 15 -- Changed by space spinner, a smaller number means less space between points, thus increasing accuracy.
Local tk2
Local LastPt
Local c = undefined
Local Altitude = 64.0

on spdSpn changed val do TopSpeed = val
on accSpn changed val do acc = val
on Spcspn changed val do AngleDiv = val
on recpath changed state do if state then Spcspn.enabled = true else Spcspn.enabled = false

fn StartSpline = (
		c = splineshape()
		addnewspline c
		addknot c 1 #corner #curve FPCam.pos
		LastPt = FPCam.pos
		bub = FPCam.pos
)

fn StopSpline = (
		addknot c 1 #corner #curve FPCam.pos;
		bub = (getOutVec c 1 ((numKnots c 1) - 1) - (getKnotPoint c 1 ((numKnots c 1) - 1)))
		setOutVec c 1 ((numKnots c 1) - 1) ((getKnotPoint c 1 ((numKnots c 1) - 1)) - bub*((distance (LastPt) FPCam.pos)/3))
		c.pivot = c.center
		local Dname = dummy pos:[0,0,0] prefix:"Camera Path "
		c.name = Dname.name
		delete Dname
		c.rotation = quat 0 0 0 1
		c.scale = [1,1,1]
		c.renderable = true	-- Sets line to be renderable in General properties
		c.thickness = 2		-- Sets thickness in General properties
		Updateshape c
		if (numKnots c 1 == 2 and (getKnotPoint c 1 1 == getKnotPoint c 1 2)) do delete c
		)

fn Rotation_Ctrl r = (if not keyboard.shiftPressed then (return r/abs(r)*TopRot)else return r/abs(r)*(TopRot/4))

fn RotationFilter L = ( -- Stops camera from flipping upside-down
if L < 0 do (if abs(L) > 90 then L = 180 else L = 0)
Return L
)

tool FstPersonCam (
	local screendist = [0,0]
	local oldposx
	local oldposy
	Local MoveOk = false
		
	on mousePoint pointno do (
		if pointno == 1 then ((oldposx = mouse.screenpos.x;oldposy = mouse.screenpos.y))
			else (if MoveOk then MoveOk = False else MoveOk = true)
		if pointno > 1 do (if CamInView != undefined then (if viewport.numviews >= CamInView do viewport.activeviewport = CamInView) else viewport.activeviewport = viewport.numviews)
	)
	
	on mousemove clickno do
	(
		Local sh2 = (TopSpeed*ForwardDueToMouse),sh3 = (TopSpeed*0.15) -- These set how much slower the camera moves forward due to mouse movement, sh2 is normal forward, sh3 is slow
		Local newposx = mouse.screenpos.x
		Local newposy = mouse.screenpos.y
		Local Invec,OutVec,dist,pkt
		camtransmtx = FPCam.objecttransform
		screendist = [newposx - oldposx,newposy - oldposy]
		if MoveOk do (
				if recpath.checked do (
					tk2 += 1
					if keyboard.ControlPressed do (if (mod tk2 AngleDiv) == 0 do (
						dist = ((distance (LastPt) FPCam.pos)/3)
						in Coordsys camtransmtx (InVec = [0,0,dist]; OutVec = [0,0,1]
						addknot c 1 #beziercorner #curve [0,0,0] InVec OutVec);LastPt = FPCam.pos
						Updateshape c; 
						pkt = (getKnotPoint c 1 ((numKnots c 1) - 1))
						bub = (getOutVec c 1 ((numKnots c 1) - 1) - pkt)
						setOutVec c 1 ((numKnots c 1) - 1) (pkt - bub*dist) )))
				if (Ctrlkey and Altkey) or (Shiftkey and Altkey) or (not Altkey) do if abs(newposx - deskcenter.x) <= deskSize.x*0.45 do (FPCam.rotation.z_rotation -= (screendist.x*RotSensitivity)) -- left right
				if (not AltKey) do (FPCam.rotation.x_rotation -= (screendist.y*RotSensitivity);if NoFlipping do (FPCam.rotation.x_rotation = RotationFilter (FPCam.rotation.x_rotation))) --up down
				if (Ctrlkey and Altkey) or (Shiftkey and Altkey) do (FPCam.rotation.x_rotation = 90 )
				if CtrlKey and not Shiftkey do (in Coordsys camtransmtx (move FPCam [0,0,(-1)*sh2]);if AltLockBox.checked do FPCam.pos.z = Altitude) --move forward --if sh < sh2 do (sh += acc; sh2 = sh)
				if Shiftkey and not Ctrlkey do (in Coordsys camtransmtx (move FPCam [0,0,sh2]);if AltLockBox.checked do FPCam.pos.z = Altitude)      --move back
				if (Altkey and not Ctrlkey and not Shiftkey) do (in Coordsys camtransmtx (move FPCam [(screendist.x*2),(-2)*(screendist.y),0]);if AltLockBox.checked do Altitude = FPCam.pos.z)
				if (Ctrlkey and Shiftkey) do (if sh < sh3 do (sh += acc; sh3 = sh);in Coordsys camtransmtx (move FPCam [0,0,(-1)*sh3]);if AltLockBox.checked do FPCam.pos.z = Altitude) -- slow forward
			)
		oldposx = newposx
		oldposy = newposy
		)
		
	on mouseAbort abortno do if abortno > 0 do (StartCam.state = off;clock.active = false;if recpath.checked do (StopSpline();print "Recording stopped");ambientColor = AmbCol;if vwdisable.checked do (for i = 1 to viewport.numviews do (viewport.activeviewport = i;if viewport.getcamera() != FPCam do max vpt disable));#stop)
)

on StartCam changed state do (if state == on then
	(CamInView = undefined
	FPCam = undefined
	if viewport.numviews == 0 do (print "No Views Found"; return ok)
	AmbCol = ambientColor
	ambientColor = color 140 140 140
	if cameras != undefined do (for i = 1 to cameras.count do if cameras[i].name == "Handy Cam" do FPCam = cameras[i])
	if FPCam == undefined do FPCam = (freecamera rotation:(angleaxis 90 [1,0,0]) pos:[0,0,64] fov:FldView clipManually:on nearclip:3 farclip:Clipdist name:"Handy Cam")
	if AltLockBox.checked do Altitude = (FPCam.pos.z)
	for i = 1 to viewport.numviews do (viewport.activeviewport = i;if viewport.getcamera() == FPCam then CamInView = i else if vwdisable.checked do max vpt disable)
	if CamInView != undefined then (viewport.activeviewport = CamInView) else (viewport.activeviewport = viewport.numviews;viewport.setcamera FPCam;max vpt disable)
	sh = MinSpeed;
	camtransmtx = FPCam.objecttransform
	clock.active = true;if recpath.checked do (tk = 0;tk2 = 0;StartSpline();Print "Recording started"); startTool FstPersonCam) else (clock.active = false;if recpath.checked do (StopSpline();print "Recording stopped"); stopTool FstPersonCam))

on clock tick do (Local v = (mouse.screenpos - DeskCenter),Dist = (length v)
	if (keyboard.ControlPressed and not keyboard.shiftPressed) do (if sh < TopSpeed do sh += acc;if sh > TopSpeed do sh = TopSpeed;in Coordsys camtransmtx (move FPCam [0,0,(-1)*sh]);if AltLockBox.checked do FPCam.pos.z = Altitude) -- Forward
	if (not keyboard.ControlPressed and not keyboard.shiftPressed) do (if sh > MinSpeed do (sh -= 1;if sh < MinSpeed do sh = MinSpeed))
	if (keyboard.shiftPressed and not keyboard.ControlPressed) do (camtransmtx = FPCam.objecttransform;if sh < TopSpeed do sh += acc;in Coordsys camtransmtx (move FPCam [0,0,sh]);if AltLockBox.checked do FPCam.pos.z = Altitude) -- Back
	if (keyboard.ControlPressed and keyboard.shiftPressed) do (camtransmtx = FPCam.objecttransform;in Coordsys camtransmtx (move FPCam [0,0,(-1)*(TopSpeed*0.25)]);if AltLockBox.checked do FPCam.pos.z = Altitude) -- Slow forward
	if abs(v.x) > deskSize.x*0.45 do (FPCam.rotation.z_rotation -= Rotation_Ctrl (v.x)) -- Controls speed of rotation when at screen edges.
	)
on FPCamRollout close do (StopTool FstPersonCam)
	
)

fn GetPosForRes =
(
	local wpos = [425,76]
	case (sysinfo.desktopsize).x of
	(
	1280: ()
	1600: wpos = [738,76]
	1400: wpos = [545,82]
	1024: wpos = [302,0]
	1152: wpos = [302,76]
	1360: wpos = [504,76]
	default: wpos = [0,0]
	)
wpos
)
--[115,66] for max 4 and 3
undo off (
local Wpos = (GetPosForRes());
	local WSize = [115,76]
	if (maxVersion())[1] < 5000 do (WSize = [115,66])
CreateDialog FPCamRollout Width:(WSize.x) height:(WSize.y) Pos:(GetPosForRes()) style:#(#style_sysmenu, #style_toolwindow)
)
-- End of Script