--------------------------------------------------------
--------------------------------------------------------
DeepValley DEMO version  1.0.1 
MAXscript for 3D Studio MAX R3.x and 3dsmax4
Copyright (c) 2001 by COREVISION
E-mail:v-logic@try-net.or.jp
http://www.ann.hi-ho.ne.jp/inforest/
--------------------------------------------------------
--------------------------------------------------------

----------------------------
SHORT DESCRIPTION
----------------------------
This script is a DEMO version of �hDeepValley�h which generates realistic terrain 
objects.It's a utility script for 3D Studio MAX R3.x and 3dsmax4.
You can create various complicated terrain objects easily with DeepValley.


----------------------------
INSTALLATION 
----------------------------
1. Put "DeepValley DEMO.mse" into your MAX\SCRIPTS directory
2. Put "DeepValley.bmp"  and "Erosion.bmp" into your MAX\SCRIPTS directory
3. Go to the Utilities panel
4. Click MAXscript
5. Click Run Script and select "DeepValley DEMO.mse"
5. Select "DeepValley DEMO" in the drop-down list
6. "DeepValley DEMO" dialogue will appear


----------------------------
USAGE
----------------------------
1. Press "Create DeepValley Object" button.
2. Edit parameters.
3. Press "Start Calculationg Erosion" button to start erosion process.
4. DeepValley object will be deleted after erosion process in this DEMO version.


----------------------------
DISCLAIMER 
----------------------------
Even though this script has been tested over and over, I cannot be personally held
responsible if something horrible happens to your computer system.
Use this script at your own risk.



