/*
Architectural Glass v 0.6 for 3DS MAX4
by Jon Seagull (jon@jonseagull.com)

Thanks to Larry Minton for providing a crucial workaround, and Swami and Bobo for abundant help.
Supersampling controls from Rayglass.ms by Alexander E. Bicalho

A streamlined interface for creating architectural glass using the standard material, 
including faked frosted glass.  Includes automatic creation of common refmap types with nested falloffs.

Known Issues:
	Opening a file containing a previous version of architectural glass in the scene crashes max.  
		You can, however, merge the objects in just fine.
	OS dings all the time when doing stuff to the material.  Anyone know why?
	Due to a bug in Max's Thin Wall Refract map, Frosted Glass does not work properly with render region, crop, or blowup.	
	The Reflection map mapbutton does not update cleanly when adding presets.  Going into and out of the map fixes this.
  	Although the raytrace map is included in the presets, bear in mind that it is purported to be significantly slower than
		 	the raytrace material with identical geometry.
	Hitting cancel when selecting a bitmap using the presets gives an ugly but harmless error message.

Future Plans:
	Add reflection tinting via color correct.
	Make the rollouts remember their open/closed states when switching back to the material
	Make the material show the medit slot background, may not be possible.

Version History:
	0.6 - Added bump map slot, preset reflection maps are now named on creation, interface changes.
	0.5 - Added automatic generation of falloff maps, first distributed version.
 	0.4 - Added Reflection map presets, Minor cosmetic fixes. 
 	0.3 - First fully-functional version.
*/

plugin material jonGlass
name:"Architectural Glass" 
classID:#(0x82ad480c, 0x81330115)
extends:Standard replaceUI:true version:6 
(
--------------------------------- Parameter Blocks --------------------------------
parameters main rollout:params 
	( 
	--Larry's code prevents an error with the frosted glass not rendering
	del type:#material subAnim:false
	--End Larry's code
	
	trans type:#float default:18 ui:trans 
	col type:#color default:[128,128,128] ui:col
	twosided type:#boolean default:false ui:twosided
	refmap type:#texturemap ui:refmap
	refmap_enable type:#boolean ui:refmap_enable default:false
	refmap_amount type:#percent ui:refmap_amount default:30
	rd_enable type:#boolean ui:rd_enable
	rd_dim_lvl type:#float ui:rd_dim_lvl default:0
	rd_ref_lvl type:#float ui:rd_ref_lvl default:3 
	bmpmap type:#texturemap ui:bmpmap
	bmpmap_enable type:#boolean ui:bmpmap_enable default:false
	bmpmap_amount type:#percent ui:bmpmap_amount default:5
	
		on trans set val do delegate.opacity = val 
		on col set val do delegate.filterColor = val
		on twosided set val do delegate.twosided = val
		on refmap set val do
			(
			delegate.reflectionMap = val
			delegate.reflectionMapEnable = true
			)
		on refmap_enable set val do delegate.reflectionMapEnable = val
		on refmap_amount set val do delegate.reflectionMapAmount = val
		on rd_enable set val do delegate.applyReflectionDimming = val
		on rd_dim_lvl set val do delegate.dimLevel = val
		on rd_ref_lvl set val do delegate.reflectionLevel = val
		on bmpmap set val do
			(
			delegate.bumpMap = val
			delegate.BumpMapEnable = true
			)
		on bmpmap_enable set val do delegate.BumpMapEnable = val
		on bmpmap_amount set val do delegate.BumpMapAmount = val

	)

parameters frost rollout:frost
	(
	frost_e type:#boolean default:false ui:frostbutton
	amt type:#percent default:40 ui:amt_ui
	blur type:#float default:7 ui:blur_ui
	envmap type:#boolean default:true ui:envmap_ui
	frame type:#integer default:2 ui:frame_ui
		on frost_e set val do
			(
			delegate.refractionMapEnable = val
			)
		on amt set val do delegate.refractionMapAmount = val
		on blur set val do delegate.refractionMap.blur = val
		on envmap set val do delegate.refractionMap.useEnviroment = val --typo is necessary.  damn programmers.
		on frame set state do 
			(
			case state of 
				(
				1: delegate.refractionMap.frame = 0
				2: delegate.refractionMap.frame = 1
				)
			)
	)

parameters color rollout:color
	(
	diff type:#color default:black ui:diff
	spec type:#color default:white ui:spec
	speclvl type:#percent default:30 ui:speclvl
	gloss type:#percent default:80 ui:gloss
	Quality type:#float default:0.5 ui:qt
	Threshold type:#float default:0.1 ui:tr

		on diff set val do delegate.diffuse = val
		on spec set val do delegate.specular = val
		on speclvl set val do delegate.specularLevel = val
		on gloss set val do delegate.glossiness = val
		on Quality set val do delegate.samplerQuality = val
		on Threshold set val do delegate.samplerAdaptThreshold = val
	)
----------------------------------------- End Parameter Blocks -------------------------------------
-------------------------------------------- Rollouts ---------------------------------------------- 
rollout params "Basic Parameters" width:326 height:227
(
	spinner Trans "Opacity: " pos:[18,9] width:80 height:16 type:#integer fieldwidth:30
	colorPicker col "Filter color: " pos:[114,3] width:104 height:30
	checkbox twoSided "2-sided" pos:[247,10] width:58 height:14
	
	label refmap_label "Reflect Map: " pos:[11,52] width:64 height:13
	checkbox refmap_enable "" pos:[79,52] width:16 height:15 
	spinner refmap_amount "" pos:[96,52] width:48 height:16 range:[0,100,80] type:#integer fieldwidth:30 
	mapButton refmap "" pos:[150,50] width:162 height:21
	
	label preset_label1 "Map Type:" pos:[16,92] width:51 height:14 
	dropdownList refmap_types "" pos:[72,88] width:150 height:21 items:#("Bitmap", "Flat Mirror", "Raytrace", "Reflect/Refract", "Mix (Empty)", "Composite (Empty)", "None (Falloffs Only)")
	label preset_label2 "Falloffs:" pos:[25,120] width:36 height:13 
	checkbox sl_falloff "Shadow/Light" pos:[69,120] width:91 height:15 
	checkbox angle_falloff "Angle:" pos:[164,120] width:54 height:15
	radiobuttons angle_falloff_type "" pos:[218,113] width:87 height:32 labels:#("Perp./Parallel", "Fresnel") columns:1 
	button apply_presets "Apply Presets" pos:[227,87] width:78 height:21
	
	checkbox rd_enable "Apply" pos:[21,162] width:50 height:15
	spinner rd_dim_lvl "Dim Level: " pos:[85,163] width:99 height:16 range:[0,1,0] scale:0.01 fieldwidth:34
	spinner rd_ref_lvl "Refl. Level: " pos:[200,164] width:103 height:16 range:[0,10,3] scale:0.01 fieldwidth:34
	
	label bmpmap_label "Bump Map: " pos:[13,200] width:58 height:13 
	checkbox bmpmap_enable "" pos:[83,200] width:14 height:15
	spinner bmpmap_amount "" pos:[102,200] width:46 height:16 range:[0,100,5] type:#integer fieldwidth:30
	mapButton bmpmap "" pos:[152,198] width:162 height:21 
	
	GroupBox grp1 "Presets" pos:[12,75] width:300 height:72
	GroupBox grp2 "Reflection Dimming" pos:[13,149] width:300 height:36
	GroupBox grp3 "Reflection Setup" pos:[6,31] width:312 height:160





	on params open do
	(
	if delegate.reflectionmap != undefined then 
		(
		refmap_enable.checked = delegate.ReflectionMapEnable
		)
	else
		(
		refmap_enable.checked = false
		)
	if delegate.bumpmap != undefined then 
		(
		bmpmap_enable.checked = delegate.BumpMapEnable
		)
	else
		(
		bmpmap_enable.checked = false
		)
	
	angle_falloff_type.enabled = angle_falloff.checked
	)

	on refmap picked do
	(
	refmap_enable.checked = delegate.ReflectionMapEnable
	)

	on angle_falloff changed state do
	(
	angle_falloff_type.enabled = state
	)

	on apply_presets pressed do
	(
	
	local ok_to_proceed = false
	local main_map
	local the_map
	local sl_falloff_map
	local angle_falloff_map
	local the_bitmap
	local falloffs_array = #(sl_falloff.checked , angle_falloff.checked)
	local angle_falloff_choice = angle_falloff_type.state
	
	-- Warning system to avoid accidentally killing existing maps
	
	if delegate.reflectionmap != undefined then 
		(
		if querybox "This material already has a reflection map.\n Are you sure you want to replace it?  This is NOT undoable." title:"Replace Reflection Map" then ok_to_proceed=true
		)
	else 
		(
		ok_to_proceed = true
		)
	
	--prevent it from trying to create an empty map, which throws an exception
	
	if ((falloffs_array[1] == false) and (falloffs_array[2] == false)) and (refmap_types.selection == 7) then
		(
		messagebox "Nothing to create!\nCommand canceled."
		ok_to_proceed = false
		)
		
	format "ok_to_proceed: %\n" ok_to_proceed
	
	case ok_to_proceed of
		(
		true:
			(
	
			case refmap_types.selection of
				(
				1:	--Bitmap
					(
					the_bitmap = selectbitmap()
					main_map = bitmaptexture bitmap:the_bitmap mappingtype:1 name:"Refmap" mapping:0 --default to spherical environment
					)
				2:	--Flat Mirror
					(
					main_map = flatmirror name:"Refmap"
					)
				3:	--Raytrace
					(
					main_map = raytrace name:"Refmap"
					)
				4:	--Reflect/Refract
					(
					main_map = reflect_refract name:"Refmap"
					)
				5:	--Mix
					(
					main_map = mix name:"Refmap" 
					)
				6:	--Composite
					(
					main_map = compositetexture name:"Refmap"
					)
				7:	--Falloffs Only
					(
					)
				)
			
			-- falloff processing 
			case falloffs_array[2] of
				(
				true: -- with angle falloff
					(
					case falloffs_array[1] of
						(
						true: -- and with shadow/Light
							(
							sl_falloff_map = falloff type:3 name:"Shadow/Light"
							if main_map != undefined then
								(
								sl_falloff_map.map2 = main_map
								sl_falloff_map.map2on = true
								)
							
							angle_falloff_map = falloff type:angle_falloff_choice map2:sl_falloff_map map2On:true name:"Angle"
							
							the_map = angle_falloff_map
							)
						false: -- no shadow/light
							(
							angle_falloff_map = falloff type:angle_falloff_choice name:"Angle"
							if main_map != undefined then
								(
								angle_falloff_map.map2 = main_map
								angle_falloff_map.map2on = true
								)
								
							the_map = angle_falloff_map
							)
						)
					)
				false: -- without angle falloff
					(
					case falloffs_array[1] of
						(
						true: -- with shadow/Light
							(
							sl_falloff_map = falloff type:3 name:"Shadow/Light"
							if main_map != undefined then
								(
								sl_falloff_map.map2 = main_map
								sl_falloff_map.map2on = true
								)
								
							the_map = sl_falloff_map
							)
						false: -- no falloffs at all
							(
							the_map = main_map
							)
						)
					)
				--end falloff cases
				)
			
			-- and now finish up
			delegate.reflectionmap = the_map
			delegate.reflectionmapenable = true
			refmap.map = delegate.reflectionmap
			refmap.text = refmap.map as string
			refmap_enable.checked = delegate.ReflectionMapEnable
			)
		false:
			(
			refmap_enable.checked = delegate.ReflectionMapEnable
			)
		--End case ok_to_proceed
		)
	--End apply_presets pressed
	)

	on bmpmap picked do
	(
	bmpmap_enable.checked = delegate.bumpmapenable
	)
)


rollout color "Other Parameters" width:#mtleditor
	(
	colorpicker diff "Diffuse (Milkiness): " align:#left across:2
	spinner speclvl "Specular Level: " type:#integer range:[0,999,50] align:#right fieldwidth:32
	colorpicker spec "Specular Color: " align:#left across:2 offset:[16,0]
	spinner gloss "Glossiness: " type:#integer range:[0,100,80] align:#right fieldwidth:32
--Alexander's code
	group "Supersampling"
		(
			checkbox ss "Enable"
			checkbox adt "Adaptive"
			dropdownlist samplers items:#("Adaptive Halton","Adaptive Uniform","Hammersley","MAX 2.5 Star") selection:4 width:100 offset:[80,-40]
			spinner qt "Quality" fieldwidth:40 offset:[0,-25] range:[0,1,0.5] scale:0.01
			spinner tr "Thresh." fieldwidth:40 range:[0,1,0.5] scale:0.001
		)

	on ss changed state do	
		(
		delegate.samplerEnable = state
		)
	
	on adt changed state do
		(
		delegate.samplerAdaptOn = state
		)
	
	on samplers selected i do
		(
		case i of
			(
			1:	(
				adt.enabled = true
				qt.enabled = true
				tr.enabled = true
				)
			2:	(
				adt.enabled = true
				qt.enabled = true
				tr.enabled = true
				)
			3:	(
				adt.enabled = false
				qt.enabled = true
				tr.enabled = false
				)
			4:	(
				adt.enabled = false
				qt.enabled = false
				tr.enabled = false
				)
			)
		delegate.sampler = i-1
		)
	
	on color open do
		(
		samplers.selection = delegate.sampler+1
		adt.checked = delegate.samplerAdaptOn
		ss.checked = delegate.samplerEnable
		case samplers.selection of
			(
			1:	(
				adt.enabled = true
				qt.enabled = true
				tr.enabled = true
				)
			2:	(
				adt.enabled = true
				qt.enabled = true
				tr.enabled = true
				)
			3:	(
				adt.enabled = false
				qt.enabled = true
				tr.enabled = false
				)
			4:	(
				adt.enabled = false
				qt.enabled = false
				tr.enabled = false
				)
			)
--End Alexander's Code
		color.open = false
		)
	--End rollout color
	)
	
rollout frost "Frosted Glass" width:#mtleditor
	(
	checkbutton frostbutton "Enable Frosted Glass" align:#left across:2 offset:[22,8]
	spinner amt_ui "Map Strength:  " range:[0,100,40] type:#integer fieldwidth:30 align:#right enabled:true
	spinner blur_ui "Blur Amount: " range:[0,100,5] fieldwidth:34 align:#right offset:[0,-10] enabled:true scale:.01
	checkbox envmap_ui "Show Environment Map through Glass" align:#center enabled:true
	radiobuttons frame_ui labels:#("Render Map on First Frame Only", "Render Map Every Frame") default:2 enabled:true
	
	on frostbutton changed state do
		(
		amt_ui.enabled = state
		blur_ui.enabled = state
		envmap_ui.enabled = state
		frame_ui.enabled = state
		)
	
	on frost open do
		(
		if frostbutton.checked then
			(
			amt_ui.enabled = true
			blur_ui.enabled = true
			envmap_ui.enabled = true
			frame_ui.enabled = true
			)
			else
			(
			amt_ui.enabled = false
			blur_ui.enabled = false
			envmap_ui.enabled = false
			frame_ui.enabled = false
			)
		frost.open = false
		)
	-- end rollout frost
	)
rollout credits "About" width:#mtleditor
	(
	label lbl01 "Architectural Glass Material" align:#left across:2
	label lbl03 "by Jon Seagull" align:#right
	label lbl00 "Version 0.6" align:#left across:2
	label lbl06 "jon@jonseagull.com" align:#right
	label lbl07 "Thanks: Larry Minton, Swami, Bobo, and Alexander Bicalho"
	--end rollout about
	)
----------------------------------------- End Rollouts ----------------------------------------
on create do 
	(
	-- Larry's code
	del=delegate
	-- end Larry's code
	
	--put a thin wall refract in the delegate so we avoid errors in frostedglass parameters
	
	refractmap = thin_wall_refraction name:"Frosted Glass Effect" thicknessOffset:0 bumpMapEffect:0 frame:1 blur:5
	delegate.refractionmap = refractmap
	delegate.refractionmapenable = false --uncheck the map for starters
	
	-- roll up extra colors and frosty glass - can this retain the state it was previously left in instead?
	frost.open = false
	color.open = false
	-- setup initial material 
	delegate.bumpmapamount = 5
	)
-- Larry's code
on clone old do
	(
	del=delegate
	)
-- End Larry's code

) 
