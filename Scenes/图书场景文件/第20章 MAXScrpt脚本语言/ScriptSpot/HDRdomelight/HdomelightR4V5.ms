-------------------------------------------------
--HDRdomeLight scripted plug-in V5
--by Chris Subagio csubagio@hotmail.com
--with code structure inspired by the great and mighty Borislav Petrov (Bobo)
--http://www.gfxcentral.com/bobo/

-- To use, just run the script. It creates a new light type: HDRdomeLight
-- To have it always available, copy this file to the /scripts/startup
-- directory.
-- Unfortunately due to the nature of scripted plugins, you'll find it
-- in the geometry category, NOT the lights category
-- The object has light properties at the bottom of the modifier
-- Creating the object gives you a default dome light
-- The light will take the ambient texture color of whatever material you 
-- apply to the dome, so you can use it to control the lights'
-- relative intensity and color. You can use more than one material by
-- using each successive map in the material, and setting the total number
-- of maps in the domeLight
-- The focus parameter determines the area of the dome that is included
-- in shadows calculations. 
-- The color sampler determines how accurately it takes the color for each
-- light from the material. 150-200 should be plenty accurate for most 
-- cases
-- By default the dome is actualy a Geosphere. You'll need to add a slice
-- modifier, and set it to remove bottom to get the dome. I suggest moving 
-- the slice plane down a bit, to get light coming from underneath your scene
-- simulating bounce light. The slice must be set to work on tris, and you will
-- also need a ToMesh modifier on top of the stack. 
-------------------------------------------------


plugin Geometry HDRdomeLight
name:"HDRdomeLight"
classId:#(0x89bf888f, 0x86ae823b)
extends:GeoSphere
category:"CSLights"
(

parameters HDRdomeLight rollout:HDRdomeLight_roll
(
numlights 		type:#integer 	default:12 		animatable:false 	ui:numlights
lightb	 		type:#float 	default:1 		animatable:false	ui:lightb
lightsl	 		type:#integer	default:32 		animatable:false	ui:lightsl
falloffpercent 	type:#integer 	default:50 		animatable:false 	ui:falloffpercent
lightms 		type:#integer 	default:128 	animatable:false 	ui:lightms
lightsr 		type:#integer 	default:8 		animatable:false 	ui:lightsr
lightc			type:#integer 	default:30 		animatable:false 	ui:lightc
lightds			type:#integer 	default:30 		animatable:false 	ui:lightds
mapsize			type:#integer 	default:128 	animatable:false 	ui:mapsize
mapbias			type:#float 	default:1 		animatable:false 	ui:mapbias
mapdens			type:#float 	default:1 		animatable:false 	ui:mapdens
nummaps			type:#integer	default:1		animatable:false	ui:nummaps
lightsh			type:#boolean	default:true	animatable:false	ui:lightsh
lightos			type:#boolean	default:true	animatable:false	ui:lightos
lightdi			type:#boolean	default:true	animatable:false	ui:lightdi
lighton			type:#boolean	default:true	animatable:false	ui:lighton
inc_type		type:#integer	default:1		animatable:false	
killlights		type:#float		default:0.001 	animatable:false 	ui:killlights
killshads		type:#float		default:1	 	animatable:false 	ui:killshads
expo			type:#float		default:0 		animatable:false 	ui:expo
inc_array		type:#nodeTab	tabsizevariable:true
gen_mattes		type:#boolean	default:false	animatable:false	ui:gen_mattes
mapscontrast	type:#float		default:5		animatable:false	ui:mapscontrast
generateEm		type:#boolean	default:false	animatable:false	ui:generateEm
permanentLights	type:#string	default:""
mainLight		type:#float		default:1		animatable:false	ui:mainLight		
) -- end params

rollout HDRdomeLight_roll "HDRdomeLight Parameters" width:162 height:728
(
	spinner numLights "* # Lights:" pos:[41,275] width:111 height:16 range:[3,1000,12] type:#integer scale:1
	spinner lightb "multiplier:" pos:[42,7] width:113 height:16 range:[0,30,1] type:#float scale:0.01
	spinner lightsl "saturation limit" pos:[25,344] width:127 height:16 range:[0,255,32] type:#integer scale:1
	spinner falloffpercent "Focus %:" pos:[38,368] width:114 height:16 range:[0,100,50] type:#integer scale:1 
	spinner lightms "map size:" pos:[38,158] width:114 height:16 range:[0,2048,128] type:#integer scale:16
	spinner lightsr "sample range:" pos:[25,180] width:127 height:16 range:[0,25,8] type:#integer scale:1
	spinner lightc "contrast:" pos:[44,54] width:111 height:16 range:[0,100,30] type:#integer scale:1
	spinner lightds "diffuse soften:" pos:[19,30] width:136 height:16 range:[0,100,30] type:#integer scale:1
	spinner mapsize "* color sampler:" pos:[30,321] width:122 height:16 range:[0,400,128] type:#integer scale:16
	spinner mapbias "bias:" pos:[60,203] width:92 height:16 range:[0,2,0.1] type:#float scale:0.1
	spinner mapdens "density:" pos:[46,226] width:106 height:16 range:[0,4,0.1] type:#float scale:0.1
	spinner nummaps "* No. of maps:" pos:[47,298] width:105 height:16 range:[0,20,1] type:#integer scale:1
	checkbox lightsh "Shadows" pos:[6,119] width:70 height:15 checked:true type:#boolean
	checkbox lightos "Overshoot" pos:[81,119] width:73 height:15 checked:true type:#boolean
	checkbox lightdi "Affect Specular" pos:[58,100] width:97 height:15 checked:true type:#boolean
	checkbox lighton "On" pos:[7,100] width:38 height:15 checked:true type:#boolean
	spinner killlights "Light thold:" pos:[41,441] width:112 height:16 range:[0,2,0.001] type:#float scale:0.001
	spinner killshads "Shadow thold:" pos:[25,463] width:128 height:16 range:[0,100,1] type:#float scale:1
	spinner expo "* Distro Bias:" pos:[40,486] width:113 height:16 range:[0,20,0] type:#float scale:0.1
	button show_focus "Toggle Focus Viz" pos:[10,519] width:143 height:21 toolTip:"Toggles the focus visualization sphere"
	button excluding "Exclude/Include" pos:[10,548] width:143 height:21 toolTip:"Launches exclude/inlude floater"
	checkbutton gen_mattes "Generate matte lights" pos:[10,577] width:143 height:21 toolTip:"Generates inverse mattes lights"
	button addSlice "Add slice modifier" pos:[10,606] width:143 height:21 toolTip:"Adds modifiers to slice dome"
	GroupBox grp1 "Shadows" pos:[5,141] width:152 height:112
	GroupBox grp2 "Optimization" pos:[4,422] width:153 height:86
	GroupBox grp3 "DomeliGht" pos:[4,259] width:154 height:156
	button refSphere "Make reflection sphere" pos:[10,635] width:143 height:21 toolTip:"Adds modifiers to slice dome"
	spinner mapscontrast "* Contrast" pos:[38,391] width:114 height:16 range:[0,100,5] type:#float scale:1
	button updateOnRender "Update light colors" pos:[10,665] width:143 height:21 toolTip:"Recalculates light colors from maps"
	checkbutton generateEm "Generate lights in view" pos:[10,696] width:143 height:21 toolTip:"Permanently create lights in viewport"
	spinner mainLight "main light:" pos:[38,79] width:117 height:16 range:[1,25,1] type:#float scale:0.1







	on falloffpercent changed val do
	(
		obj_name = $.name + "fallviz"
		txt = "if $" + obj_name as string + " != undefined then $" + obj_name as string + ".radius = ($.falloffpercent /100. * $.delegate.radius) \n"
		execute txt
	)-- end falloffpercent

	on show_focus pressed do
	(
		obj_name = $.name + "fallviz"
		txt = 	"if $" + obj_name as string + " == undefined then ( \n"
		txt += 	"	vizer = geosphere pos:($.position) segments:5 radius:($.falloffpercent / 100. * $.delegate.radius)\n"
		txt += 	"	vizer.parent = $	\n"
		txt += 	"	vizer.wirecolor = color 0 255 0	\n"
		txt += 	"	vizer.name = \"" + obj_name as string + "\"\n"
		txt += 	")\n"
		txt += 	"else (\n"
		txt += 	"	delete  $" + obj_name as string + "\n"
		txt += 	")\n"
		execute txt
	)-- end show_focus

	on excluding pressed do
	(
		rollout excluder "Lights include/exclude" width:210 height:366
		(
			button add_object "Add Selection" pos:[7,308] width:93 height:26
			listbox all_objects "the following objects" pos:[7,73] width:195 height:14
			radiobuttons inc_type "from" pos:[6,24] width:198 height:46 labels:#("Illumination", "Shadow casting", "Both") columns:2
			radiobuttons inc_exc "" pos:[6,6] width:145 height:16 labels:#("Include", "Exclude") columns:2
			button subtract_object "Subtract Selection" pos:[109,308] width:93 height:26
			button clearBtn "Clear" pos:[150,283] width:52 height:19
			button removeBtn "remove" pos:[90,283] width:52 height:19
			button doneBtn "Done" pos:[162,340] width:40 height:21
	
			on excluder open do
					(
			global dome = $
			for i in dome.inc_array do
				(
				append all_objects.items (i.name as string)
				all_objects.items = all_objects.items
			) -- end i
			case of
			(
				(dome.inc_type == 1) : (inc_type.state = 1 
					inc_exc.state = 1)
				(dome.inc_type == 2) : (inc_type.state = 1 
					inc_exc.state = 2)
				(dome.inc_type == 3) : (inc_type.state = 2 
					inc_exc.state = 1)
				(dome.inc_type == 4) : (inc_type.state = 2 
					inc_exc.state = 2)
				(dome.inc_type == 5) : (inc_type.state = 3 
					inc_exc.state = 1)
				(dome.inc_type == 6) : (inc_type.state = 3 
					inc_exc.state = 2)					
				) -- end case
			if inc_exc.state == 1 then inc_type.caption = "in" else inc_type.caption = "from"
			)
	
			on excluder close do
			(
				case of 
				(
					(inc_type.state == 1) : (if inc_exc.state == 1 then dome.inc_type = 1 else dome.inc_type = 2)
					(inc_type.state == 2) : (if inc_exc.state == 1 then dome.inc_type = 3 else dome.inc_type = 4)
					(inc_type.state == 3) : (if inc_exc.state == 1 then dome.inc_type = 5 else dome.inc_type = 6)
				) -- end case
				dome.inc_array = #()
				for i in all_objects.items do
				(					
					execute ("append dome.inc_array $" + i as string)
				) -- end for i 
				select dome
				modpanel.setCurrentObject $
			) -- end on close
	
			on add_object pressed do
					(
			for i in $ do (
				if (finditem all_objects.items (i.name as string)) == 0 then append all_objects.items (i.name as string)
			)
			all_objects.items = all_objects.items
			)-- end on add
	
			on inc_exc changed stat do
						if stat == 1 then inc_type.caption = "in" else inc_type.caption = "from"
	
			on subtract_object pressed do
					(
				for i in $ do (
					this_item = (finditem all_objects.items (i.name as string))
					if this_item != 0 then deleteItem all_objects.items this_item 
				)
				all_objects.items = all_objects.items
			) -- end on subtract
	
			on clearBtn pressed do
						all_objects.items = #()
	
			on removeBtn pressed do
						(deleteitem all_objects.items all_objects.selection; all_objects.items = all_objects.items)
			
			on doneBtn pressed do
						closeRolloutFloater excluderFloater
		)
		
		try (closerolloutfloater excluderFloater)
		catch()
		global excluderFloater
		excluderFloater = newRolloutFloater "Excluder" 235 427 
		addrollout excluder excluderFloater
		
	)-- end excluding

	on addSlice pressed do
	(
		addmodifier $ (Uvwmap maptype:2)
		addmodifier $ (sliceModifier Slice_Type:4 Faces___Polygons_Toggle:0) before:1
		addmodifier $ (Normalmodifier flip:true)
		addmodifier $ (Turn_to_Mesh())
		max modify mode
		modpanel.setCurrentObject $
		completeRedraw() 
	
	) -- end of addSlice

	on refSphere pressed do
	(
		obj_name = $.name + "_ref"
		txt = 	"if $" + obj_name as string + " == undefined then ( \n"
		txt += 	"	vizer = geosphere pos:($.position) segments:8 radius:($.delegate.radius*1.1)\n"
		txt += 	"	vizer.parent = $	\n"
		txt += 	"	vizer.wirecolor = color 50 50 50	\n"
		txt += 	"	vizer.name = \"" + obj_name as string + "\"\n"
		txt +=  "	addmodifier vizer (Normalmodifier flip:true)	\n"
		txt +=  "	modpanel.setCurrentObject $	"
		txt += 	")\n"
		txt += 	"else (\n"
		txt += 	"	if (querybox \"It's already there, wanna kill it?\" title:\"Sure?\") == true then delete  $" + obj_name as string + "\n"
		txt += 	")\n"
		execute txt
	) -- end of addSlice

	on updateOnRender pressed do
	(
	-- color and position sampling code
		dome=$
		colors_sample_size = dome.mapsize		 
		saturation_limit = dome.lightsl		 
		num_lights = dome.numlights		 
		num_maps = dome.nummaps	 
		maps_array = #() 	 
		sam_w = colors_sample_size * 1.5	 
		sam_h = colors_sample_size 	 
		if num_lights < dome.mesh.numverts then step_size = (dome.mesh.numverts / (num_lights)) else step_size = 1		 
			for i in 1 to num_maps do(		 
				if (dome.material!=undefined and dome.material.maps[i]!=undefined) then (	 
					case i of ( 
						1: 	(temp_map1 = renderMap dome.material.maps[1] size:[sam_w, sam_h]  
							 append maps_array temp_map1 ) 
						2: 	(temp_map2 = renderMap dome.material.maps[2] size:[sam_w, sam_h]  
							 append maps_array temp_map2 ) 
						3: 	(temp_map3 = renderMap dome.material.maps[3] size:[sam_w, sam_h]  
							 append maps_array temp_map3 ) 
						4: 	(temp_map4 = renderMap dome.material.maps[4] size:[sam_w, sam_h]  
							 append maps_array temp_map4 ) 
						5: 	(temp_map5 = renderMap dome.material.maps[5] size:[sam_w, sam_h]  
							 append maps_array temp_map5 ) 
						6: 	(temp_map6 = renderMap dome.material.maps[6] size:[sam_w, sam_h]  
							 append maps_array temp_map6 ) 
						7: 	(temp_map7 = renderMap dome.material.maps[7] size:[sam_w, sam_h]  
							 append maps_array temp_map7 ) 
						8: 	(temp_map8 = renderMap dome.material.maps[8] size:[sam_w, sam_h]  
							 append maps_array temp_map8 ) 
						9: 	(temp_map9 = renderMap dome.material.maps[9] size:[sam_w, sam_h]  
							 append maps_array temp_map9 ) 
						10: (temp_map10 = renderMap dome.material.maps[10] size:[sam_w, sam_h]  
							 append maps_array temp_map10 ) 
					)	 
				)	 
				else (	 
					case i of ( 
						1:	(temp_map1 = bitmap sam_w sam_h color:(color 128 128 128)  
							 append maps_array temp_map1)  
						2:	(temp_map2 = bitmap sam_w sam_h color:(color 128 128 128)  
							 append maps_array temp_map2)  
						3:	(temp_map3 = bitmap sam_w sam_h color:(color 128 128 128)  
							 append maps_array temp_map3)  
						4:	(temp_map4 = bitmap sam_w sam_h color:(color 128 128 128)  
							 append maps_array temp_map4)  
						5:	(temp_map5 = bitmap sam_w sam_h color:(color 128 128 128)  
							 append maps_array temp_map5)  
						6:	(temp_map6 = bitmap sam_w sam_h color:(color 128 128 128)  
							 append maps_array temp_map6)  
						7:	(temp_map7 = bitmap sam_w sam_h color:(color 128 128 128)  
							 append maps_array temp_map7)  
						8:	(temp_map8 = bitmap sam_w sam_h color:(color 128 128 128)  
							 append maps_array temp_map8)  
						9:	(temp_map9 = bitmap sam_w sam_h color:(color 128 128 128)  
							 append maps_array temp_map9)  
						10:	(temp_map10 = bitmap sam_w sam_h color:(color 128 128 128)  
							 append maps_array temp_map10)  
					)	 
				)	 
			)				 
			 
		--- now with the getting big ol array of values from the maps	 
		pixels_arr = #()	 
		map_expo = dome.mapscontrast / 1000.	 
		for i in 0 to (sam_h-1) do (	 
			line_array = #()	 
			for j in 1 to maps_array.count do (	 
				append line_array (getpixels maps_array[j] [0,i] sam_w)	 
			) -- end j	 
			added_array = line_array[1]	 
			if line_array.count > 1 then (	 
			for j in 2 to line_array.count do(	 
				for k in 1 to sam_w do (	 
					expoed_number = copy line_array[j][k]	 
					expoed_number.v = expoed_number.v ^ (1 + map_expo*j)	 
					added_array[k] += expoed_number	 
				)	 
			)	 
			) -- end if	 
			append pixels_arr added_array	 
		) -- end i	 
		 
		light_arr = #()	 
		for i in 1 to dome.mesh.numverts do (	 
		 	tv_pos = gettvert dome.mesh i		 
			tv_pos.x = mod tv_pos.x 1  
			if tv_pos.x < 0 then tv_pos.x += 1	 
			tv_pos.y = mod tv_pos.y 1  
			if tv_pos.y < 0 then tv_pos.y += 1	 
			vert_color = pixels_arr[(	(sam_h) - ( tv_pos.y * 	(sam_h-1)	)	)][( 	tv_pos.x * (	sam_w-1	) + 1 	)]	 
			vert_mult = vert_color.v / num_maps / 255 	 
			vert_color = vert_color / num_maps	 	 	 
			vert_color.a = 0	 
			append light_arr #(((getVert dome.mesh i )*dome.transform), vert_mult, vert_color)	 
			if (mod i 150) == 0. then gc ()  
		)	 
			 
		--- sorting	 
			for x in light_arr.count to 1 by -1 do (	 
				for y in 2 to x do (	 
					if light_arr[y][2] > light_arr[y-1][2] then (swap light_arr[y] light_arr[y-1] )	 
				)	 
			)	 
		--- end	of sorting	 
	 
		-- sampling the array for the actual lights now	 
		ar2 = #()	 
		ar3 = #()	 
		ar4 = #()	 
		dist_exp = dome.expo	 
		if num_lights < dome.mesh.numverts then (	 
		steps = dome.mesh.numverts *1.0/ num_lights	 
		)	 
		else (	 
		steps = 1.	 
		)	 
		for i = 1 to dome.mesh.numverts by steps do (	 
			append ar2 i	 
			)	 
		for j = 1 to ar2.count do	 
			(	 
			m = (( (ar2[j] - 1) * 1.0 / (dome.mesh.numverts - 1) ) ^ (1 + dist_exp/10.) * (dome.mesh.numverts - 1) + 1 ) as integer	 
			append ar3 m 	 
			)	 
			 
		for k in 1 to ar3.count do 	 
			(	 
			if finditem ar4 ar3[k] == 0 then 	 
				(	 
				append ar4 ar3[k]	 
				)	 
			)	 
		final_lights = #()	 
		total_energy = 0.0	 
		for i in 1 to ar4.count do(	 
			append final_lights light_arr[ar4[i]]	 
			total_energy += light_arr[ar4[i]][2]	 
		)	 
		for i in final_lights do(	 
			i[2] = i[2]/total_energy	 
		)	 
		toS = ""	 
		for each_light in final_lights do(	 
			tos += each_light[1].x as string + " "	 
			tos += each_light[1].y as string + " "	 
			tos += each_light[1].z as string + " "	 
			tos += each_light[2] as string + " "		 
			tos += each_light[3].r as string + " "	 
			tos += each_light[3].g as string + " "	 
			tos += each_light[3].b as string + " "		 
		)	 
		dome.permanentLights = toS	  
	
	-- finished sampling
	) -- end on updateOnRender

	on generateEm changed state do
	(
		
			if state == on then 
			(
	
			dome = $
			dome.wirecolor = (color 255 255 0)	 
	 		dome.renderable = false 	 
	 		dome.castshadows = false 	 
	 		c_shadows = dome.lightsh		 
	 		m_ss = dome.lightms		 
	 		m_sr = dome.lightsr		 
	 		m_c = dome.lightc		 
	 		m_ds = dome.lightds		 
	 		m_f = dome.delegate.radius * dome.falloffpercent/100		 
	 		m_os = dome.lightos		 
	 		m_di = dome.lightdi		 
	 		m_bi = dome.mapbias		 
	 		m_de = dome.mapdens		 
	 		master_multiplier = dome.lightb * 4.0		 
	 		mattesArray = #()	 
	
	-- reconstruct the final_lights array
	
	 		holdar = filterstring dome.permanentLights  " "	 
	 		if holdar.count > 0 then (	 
	
		 		final_lights = #()	 
		 		for i in 1 to (holdar.count/7) do (	 
		 			l_position = [ (holdar[(i-1)*7+1] as float), (holdar[(i-1)*7+2] as float), (holdar[(i-1)*7+3] as float)]	 
		 			l_color	= color (holdar[(i-1)*7+5] as float) (holdar[(i-1)*7+6] as float) (holdar[(i-1)*7+7] as float)	 
		 			rer = #()	 
		 			append rer l_position	 
		 			append rer (holdar[(i-1)*7+4] as float)	 
		 			append rer l_color	 
		 			append final_lights rer	 
		 		)	 
		
	-- begin making lights
			
	 			if dome.gen_mattes == true then (	 
	 				for obj in objects where classof obj.material == MatteShadow do append mattesArray obj	 
	 			) -- end if gen_mattes 	 
				first_light = 0
	 			for i in final_lights do(	 
	 				if (i[2] * master_multiplier) > dome.killlights then (	 
	 					new_light = targetDirectionalLight pos:(i[1])	target:(targetObject pos:dome.position)	 
	 					new_light.name = uniquename ("spot" + dome.name as string + "_")
						if i[3].s > dome.lightsl then i[3].s = dome.lightsl		 
	 					new_light.color = i[3]	 
	 					new_light.multiplier = i[2] * master_multiplier	 
						if first_light == 0 then new_light.multiplier = new_light.multiplier*dome.mainLight
						first_light+=1
	 					new_light.diffuse_soften = m_ds	 	 
	 					new_light.contrast = m_c			 	 
	 	 				new_light.overshoot = true 		 	 
	 					new_light.hotspot = m_f - 2		 	 
	 					new_light.falloff = m_f	 	 
	 					if (i[2]*100) > dome.killshads then (  
	  						new_light.baseobject.castshadows = c_shadows			 		 
	  					)	 
	  					else (new_light.baseobject.castshadows = false)	 
	  					new_light.mapsize = m_ss		 	 
	  					new_light.samplerange = m_sr			 		 
	  					new_light.overshoot = m_os			 		 
	  					new_light.affectspecular = m_di			 		 
	  					new_light.mapbias = m_bi			 		 
	  					new_light.shadowdensity = m_de	 
	  					new_light.parent = dome	 
	  					new_light.target.parent = dome	 
	 					if dome.gen_mattes == true and new_light.baseobject.castshadows == true then (	 
	 							mnew_light = copy new_light		 
	 							new_target = copy new_light.target	 
	 							mnew_light.target = new_target	 
	 							mnew_light.name = uniquename ("spot" + dome.name as string + "_")	 
	 							mnew_light.color = black	 
	 							mnew_light.baseobject.castshadows = true	 
	  							mnew_light.mapsize = m_ss		 	 
	  							mnew_light.samplerange = m_sr			 		 
	 							mnew_light.shadowcolor = i[3]	 
	 							mnew_light.includelist = mattesArray	 
	  							mnew_light.mapbias = m_bi			 		 
	  							mnew_light.shadowdensity = new_light.multiplier * m_de	 
	 					) -- end if	 
	 					if dome.inc_array.count != 0 then (	 
	 						assign_array = #()	 
	 						for arr_item in dome.inc_array do append assign_array arr_item	 
	 						case dome.inc_type of (	  
	 							(1): (new_light.inclExclType = 1 ; new_light.includeList = assign_array)	 
	 							(2): (new_light.inclExclType = 1 ; new_light.excludeList = assign_array)	 
	 							(3): (new_light.inclExclType = 2 ; new_light.includeList = assign_array)	 
	 							(4): (new_light.inclExclType = 2 ; new_light.excludeList = assign_array)	 
	 							(5): (new_light.inclExclType = 3 ; new_light.includeList = assign_array)	 
	 							(6): (new_light.inclExclType = 3 ; new_light.excludeList = assign_array)	 
	 						)-- end of case	 
	 					) -- end of if	 
	 				)-- end of if
	 			)-- end of i loop	 
	 		) -- end holdar if 
	  		) -- end if
			else
			(
			temp_string = "delete $spot" + $.name as string + "_" + "*"
			execute temp_string
			)
		) -- end generate em
)

		
		
on create do
(
this.delegate.mapCoords = true
this.delegate.segments = 10

callbacks.removescripts id:#HDRdomeLight

-- pre render
txt = " \n"
txt += "EscapeEnable = False \n"
txt += "--delete $spot_HDRdome* \n"
txt += "	for dome in $HDRdome* where (dome.lighton == true and dome.generateEm == false) do (	\n"
txt += "		dome.wirecolor = (color 255 255 0)	\n"
txt += "		dome.renderable = false 	\n"
txt += "		dome.castshadows = false 	\n"
txt += "		c_shadows = dome.lightsh		\n"
txt += "		m_ss = dome.lightms		\n"
txt += "		m_sr = dome.lightsr		\n"
txt += "		m_c = dome.lightc		\n"
txt += "		m_ds = dome.lightds		\n"
txt += "		m_f = dome.delegate.radius * dome.falloffpercent/100		\n"
txt += "		m_os = dome.lightos		\n"
txt += "		m_di = dome.lightdi		\n"
txt += "		m_bi = dome.mapbias		\n"
txt += "		m_de = dome.mapdens		\n"
txt += "		master_multiplier = dome.lightb * 4.0		\n"
txt += "		mattesArray = #()	\n"

-- reconstruct the final_lights array

txt += "		holdar = filterstring dome.permanentLights \" \"	\n"
txt += "		if holdar.count > 0 then (	\n"

txt += "		final_lights = #()	\n"
txt += "		for i in 1 to (holdar.count/7) do (	\n"
txt += "			l_position = [ (holdar[(i-1)*7+1] as float), (holdar[(i-1)*7+2] as float), (holdar[(i-1)*7+3] as float)]	\n"
txt += "			l_color	= color (holdar[(i-1)*7+5] as float) (holdar[(i-1)*7+6] as float) (holdar[(i-1)*7+7] as float)	\n"
txt += "			rer = #()	\n"
txt += "			append rer l_position	\n"
txt += "			append rer (holdar[(i-1)*7+4] as float)	\n"
txt += "			append rer l_color	\n"
txt += "			append final_lights rer	\n"
txt += "		)	\n"

-- begin making lights

txt += "		if dome.gen_mattes == true then (	\n"
txt += "			for obj in objects where classof obj.material == MatteShadow do append mattesArray obj	\n"
txt += "		) -- end if gen_mattes 	\n"
txt += "		first_light=0	\n"
txt += "		for i in final_lights do(	\n"
txt += "			if (i[2] * master_multiplier) > dome.killlights then (	\n"
txt += "				new_light = targetDirectionalLight pos:(i[1])	target:(targetObject pos:dome.position)	\n"
txt += "				new_light.name = uniquename (\"spot\" + dome.name as string + \"_\")		\n"
txt += "				if i[3].s > dome.lightsl then i[3].s = dome.lightsl		\n"
txt += "				new_light.color = i[3]	\n"
txt += "				new_light.multiplier = i[2] * master_multiplier	\n"
txt += "				if first_light == 0 then new_light.multiplier = new_light.multiplier*dome.mainLight	\n"
txt += "				first_light += 1	\n"
txt += "				new_light.diffuse_soften = m_ds	 	\n"
txt += "				new_light.contrast = m_c			 	\n"
txt += " 				new_light.overshoot = true 		 	\n"
txt += "				new_light.hotspot = m_f - 2		 	\n"
txt += "				new_light.falloff = m_f	 	\n"
txt += "				if (i[2]*100) > dome.killshads then ( \n"
txt += " 					new_light.baseobject.castshadows = c_shadows			 		\n"
txt += " 				)	\n"
txt += " 				else (new_light.baseobject.castshadows = false)	\n"
txt += " 				new_light.mapsize = m_ss		 	\n"
txt += " 				new_light.samplerange = m_sr			 		\n"
txt += " 				new_light.overshoot = m_os			 		\n"
txt += " 				new_light.affectspecular = m_di			 		\n"
txt += " 				new_light.mapbias = m_bi			 		\n"
txt += " 				new_light.shadowdensity = m_de	\n"
txt += " 				new_light.parent = dome	\n"
txt += " 				new_light.target.parent = dome	\n"
txt += "				if dome.gen_mattes == true and new_light.baseobject.castshadows == true then (	\n"
txt += "						mnew_light = copy new_light		\n"
txt += "						new_target = copy new_light.target	\n"
txt += "						mnew_light.target = new_target	\n"
txt += "						mnew_light.name = uniquename (\"spot\" + dome.name as string + \"_\")	\n"
txt += "						mnew_light.color = black	\n"
txt += "						mnew_light.baseobject.castshadows = true	\n"
txt += " 						mnew_light.mapsize = m_ss		 	\n"
txt += " 						mnew_light.samplerange = m_sr			 		\n"
txt += "						mnew_light.shadowcolor = i[3]	\n"
txt += "						mnew_light.includelist = mattesArray	\n"
txt += " 						mnew_light.mapbias = m_bi			 		\n"
txt += " 						mnew_light.shadowdensity = new_light.multiplier * m_de	\n"
txt += "				) -- end if	\n"
txt += "				if dome.inc_array.count != 0 then (	\n"
txt += "					assign_array = #()	\n"
txt += "					for arr_item in dome.inc_array do append assign_array arr_item	\n"
txt += "					case dome.inc_type of (	\n" 
txt += "						(1): (new_light.inclExclType = 1 ; new_light.includeList = assign_array)	\n"
txt += "						(2): (new_light.inclExclType = 1 ; new_light.excludeList = assign_array)	\n"
txt += "						(3): (new_light.inclExclType = 2 ; new_light.includeList = assign_array)	\n"
txt += "						(4): (new_light.inclExclType = 2 ; new_light.excludeList = assign_array)	\n"
txt += "						(5): (new_light.inclExclType = 3 ; new_light.includeList = assign_array)	\n"
txt += "						(6): (new_light.inclExclType = 3 ; new_light.excludeList = assign_array)	\n"
txt += "					)-- end of case	\n"
txt += "				) -- end of if	\n"
txt += "			)	\n"
txt += "		)-- end of i loop	\n"

txt += "		)	\n"
-- end of holdar conditional and constructing lights

txt += "		else ()	\n"
txt += "	gc () \n"

-- end for loop
txt += "	) \n"


callbacks.addScript #preRender txt id:#HDRdomeLight persistent:true

--- post render

txt = "for dome in $HDRdome* where dome.generateEm == false do (	\n"
txt += "	temp_string = \"delete $spot\" + dome.name as string + \"_\" + \"*\"	\n"
txt += "	execute temp_string	\n"
txt += "	)	\n"
txt += "gc () \n"
txt += "EscapeEnable = True\n"

callbacks.addScript #postRender txt id:#HDRdomeLight persistent:true

) -- end on create

) -- end plugin