--创建嵌入到工具面板中的窗体--
utility EffectLibs "特效库" width:162 height:252
(
	button fire " 火 焰" pos:[16,18] width:130 height:30--定义创建火焰效果的按钮
	bitmap testrender "Bitmap" pos:[16,59] width:130 height:100--定义显示预览渲染效果的窗口
	button renderview "渲 染 测 试" pos:[16,169] width:130 height:25 --定义执行预览渲染的按钮
	on fire pressed do--“火 焰”按钮执行的语句，产生火焰、大气和自由摄影机
	(
			sgizmo=SphereGizmo radius:80--创建球形大气线框
			vf=Fire_Effect()
			addAtmospheric vf--增加火焰大气特效
			appendGizmo vf sgizmo--指定球形大气线框给火焰大气特效
			cam=freecamera pos:[0,-200,0] target:(targetobject())--创建一个自由摄影机
			max vpt camera--将当前激活视图切换为摄影机视图
		)
	on renderview pressed  do--“渲 染 测 试”按钮执行的语句
(
		view=render outputwidth:130 outputheight:100 vfb:off--进行场景渲染，输出尺寸为130*100
		testrender.bitmap=view--将渲染效果显示在位图控件中
	)
--创建第二个卷展栏--
rollout param2 "参数调节" width:162 height:252
(
)
--定义第二个卷展栏到面板上--
on EffectLibs open do
(
   addRollout param2 rolledup:true
   )
--定义关闭第二个卷展栏--   
on EffectLibs close do
(
   removeRollout param2 
  )
)
