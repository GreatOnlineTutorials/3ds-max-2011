--创建嵌入到工具面板中的窗体--
utility EffectLibs "特效库" width:162 height:252
(
	button fire " 火 焰" pos:[16,18] width:130 height:30--定义创建火焰效果的按钮
	bitmap testrender "Bitmap" pos:[16,59] width:130 height:100--定义显示预览渲染效果的窗口
	button renderview "渲 染 测 试" pos:[16,169] width:130 height:25 --定义执行预览渲染的按钮
	on fire pressed do--“火 焰”按钮执行的语句，产生火焰、大气和自由摄影机
	(
	  global	sgizmo=SphereGizmo radius:80--创建球形大气线框
	  global vf=Fire_Effect()
		     addAtmospheric vf--增加火焰大气特效
			appendGizmo vf sgizmo--指定球形大气线框给火焰大气特效
			cam=freecamera pos:[0,-200,0] target:(targetobject())--创建一个自由摄影机
			max vpt camera--将当前激活视图切换为摄影机视图
		)
	on renderview pressed  do--“渲 染 测 试”按钮执行的语句
(
		view=render outputwidth:130 outputheight:100 vfb:off--进行场景渲染，输出尺寸为130*100
		testrender.bitmap=view--将渲染效果显示在位图控件中
	)
--创建第二个卷展栏--
rollout param2 "参数调节" width:162 height:252
(
	spinner spn1 "浓烈程度" pos:[21,18] width:130 height:16 enabled:true range:[0,50,15]
	slider sld1 "火焰大小" pos:[26,51] width:126 height:44 range:[0,100,80]
	on spn1 changed val do
	(
	        vf.density=spn1.value
	       (
	          --颜色随浓度变化--
	          if spn1.value<=15 then 
	         (   
	            vf.inner_color=color 252 202 0
	            vf.outer_color=color 225 30  30
	          )
	      	else if spn1.value>30 then 
	         (
	           vf.inner_color=color 253 255 94
	           vf.outer_color=color 144 126 255	
	         )
	         else 
	         (
	          vf.inner_color=color 253 255 94; 
	          vf.outer_color=color 255 129 129
	          )
	      )
	)
	on sld1 changed val do
		sgizmo.radius=sld1.value
)
--创建第三个卷展栏--
rollout param3 "温度调节" width:162 height:252
(
	
  
 )
--创建第四个卷展栏--
rollout param4 "比例调节" width:162 height:252
(
   
)
--定义其他卷展栏到面板上--
on EffectLibs open do
(
   addRollout param2 rolledup:true
   addRollout param3 rolledup:true
   addRollout param4 rolledup:true
   )
--定义关闭其他卷展栏--
on  EffectLibs close do
(
   removeRollout param2 
   removeRollout param3
   removeRollout param4
  )
)