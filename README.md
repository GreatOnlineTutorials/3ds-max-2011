     _____     _                           ____   ___  _ _ 
    |___ /  __| |___    /\/\   __ ___  __ |___ \ / _ \/ / |
      |_ \ / _` / __|  /    \ / _` \ \/ /   __) | | | | | |
     ___) | (_| \__ \ / /\/\ \ (_| |>  <   / __/| |_| | | |
    |____/ \__,_|___/ \/    \/\__,_/_/\_\ |_____|\___/|_|_|
    
    火星时代3ds Max 2011白金手册
    
    3ds Max 2011 Manual by HXSD

    《3ds Max 2011白金手册》系列是"火星时代"推出的全面介绍如何利用3ds Max软件制作三
    维动画的大型多媒体教学手册，包含四册约2400页的教学手册和8张DVD。详细信息请参阅
    Readme.txt。
    
    这是8张DVD的备份。网页和一些3ds Max源文件已用git备份到网站上，而8张DVD共726个视
    频已上传至Youtube，下面有这些视频的ID。光盘内网页中原有的指向光盘视频文件的链接已
    换上Youtube链接。
    
    Git仓库上有两个分支，其中分支master含有Scenes文件夹（包含一些3ds Max源文件），大
    小约为2GB，而另一个分支WithoutScenes则没有这个文件夹，zip压缩后大小约为87MB。
    
    利用脚本代码从各DVD盘上的video.html获取各视频的章节信息，利用这些信息作为Youtube
    视频标题和描述的参数，上传完视频后再换上这些Youtube链接。
    
    DVD 1 (106个视频):
    
    [KKHgRsCrCEA] [Tw1cXSCtAPo] [gQXWmDLBpaM] [WmX8lNwUNoY] [iCLzBDbmTuo] 
    [kHctxmZIHoM] [_9iyylcOC44] [cAAAAHx3xg8] [kAXBQMNtnAs] [nUKs8DFf_yM] 
    [736eRPmh0a4] [AV3KICda_5k] [PqRG103X0p0] [uHxoV7gcaqM] [aZ29qn5bITk] 
    [9pHY5E7lU94] [59wlaeYd7b8] [5Yi8OTlhPwo] [1z4Ytd5KP3k] [Va16-COivl0] 
    [_NKDWP_l2lw] [VY_lm0zsn1Q] [296xebx_5og] [fedKEn0rb28] [s60BuAE7I8Q] 
    [O_PjnMUezJA] [HUJ2e_BZHiU] [CVW4R6rnmJw] [tYAk5mHQqRk] [ELbu0nHbHKk] 
    [ZdrqeQT9SDU] [jhBtZdiFCis] [CfeifllGwyc] [j3Q1mBGXrvQ] [9MYwWaJrJ6U] 
    [-hhShvyGB48] [1WpruLzTUDM] [lv-3B3DKDIw] [TnZ3iOEuVZw] [6rTz6xTcXH4] 
    [9JO7Kd0UeDo] [yuktVqa44Zw] [ZUjyQ6QLis0] [eVQ5qRmOWSI] [7YL4W-uqcWA] 
    [3NVEPEZ9EKM] [P2ed4cI976Y] [6WCBG294Qk4] [llxYBxGfvRs] [_yMjMhdwiRc] 
    [NdsobE2YK-k] [ke2mBB9vXgQ] [qeFZcxn7Ayk] [QpJaK7mwWsw] [-Qk-e1N3vo0] 
    [T2w026qQJYg] [A7NABSrTiWk] [jW-JbfXgJXU] [yQXragLPqB0] [Ws0ikBj5Vr8] 
    [XjJ2ti5NU0w] [SMaHpvx9fVc] [UocxQ8MatXc] [4o4rUARAjrc] [UseRvfvrUVU] 
    [Cal3nD8zT7E] [E0g6dXDpnko] [RosPbWgQV3U] [z38PKDSUG_k] [gHdC47kGx0c] 
    [j35FAwhHXu8] [IPfY_qUH0CI] [dAP-ZL-6VaE] [E52oSIMvw9w] [Gj-798Z85zo] 
    [i4-SBhpAuFA] [yik_pi7hOeY] [UWySxJyQork] [IbRYVQcH9Hs] [uqWOMcToDKo] 
    [jBXCvUcRUMM] [M6Yikk5MdIg] [gIE2RCQk9es] [ekq2MuS-P8Y] [cXDY7_kAihc] 
    [ps6Mms5cNZQ] [45EbtgiIjIs] [pozdAB8D0Zg] [WfTfa8yEnrc] [PbyNkxV4hDA] 
    [r9SQh-z0o0Y] [eZr8d_lrr6Y] [yy6BjXoqPG0] [mroUy-eKTdc] [K9KDSj1WIpg] 
    [JPdFZbcB4u8] [sENmhNOU_ew] [X9KlSNVyoHA] [2NbQqsRLImE] [H_HTrgPtUGM] 
    [NgLmn3E1nbc] [_r4KLyPYDOo] [DWghLdr3tuo] [fYNuIr-yj6Q] [qNwdpgvCirE] 
    [kHPt63rDZXM] 
    
    DVD 2 (194个视频):

    [aVlseU5BJMU] [5WZ9vO5k1ms] [Ot9ozDSOaNM] [k9D_emUUe40] [ygU2j02Qnns] 
    [05hXOvK6yqU] [Oh4zrtF6Mc0] [ef0aaItRrt0] [Deh9vTrQKyA] [dwM_Fo9WeSU] 
    [o_Yp5lNrQzw] [FIe4s_tnVIk] [bol4vND-Ds4] [NzO7LjTfoG8] [7Dy__l2C31M] 
    [dLGmdj9eKcI] [RW7QVz_BfHQ] [oQYb4b-q4G0] [HIZ1uGk2qZs] [-7mBWTMtQZ4] 
    [8VIfG6gziag] [pghrW332N6I] [dcdAv-xgQD8] [A3g8CWA9Vag] [76WVWx23M-o] 
    [Dq-VBEx-lL4] [dkgqyN-63ho] [M3Sk4M8FNe8] [W4Zlg_FuPiA] [hCQWLB8MeNQ] 
    [VpjtQ1SpSv8] [mypFSV3oBe4] [A-Ye-m512S4] [xChLqcHiWMw] [K5GKfUdQX9I] 
    [hiazxKeZfrs] [cNlkC28CcUI] [T9fp4ARVSlc] [aVD6N8mEofs] [SPFwKUJcCQc] 
    [svbR7kQSkoc] [TspRx0i9FmM] [F9HpSoifAKw] [IKF_zDN_H44] [vUOGHZZME20] 
    [UoPns9BNw74] [T7PHjCVF3Wg] [xIX_7xxsgAI] [i_vQ-jzUgzM] [5HBrx8hXHpg] 
    [ZpeDUv9g-9o] [wnBbnYxJTHU] [hPRjR1rxhqk] [ASxaXka4LHY] [iEtduy-Ri7o] 
    [SyzgyEqRWiM] [XlkYquwgNYg] [aRHPwq7pEpc] [syxItfp5mhE] [C2rtQBNBtJI] 
    [86cDZXuLp54] [ypE4tvU2130] [asvoZ0gZGBY] [DwTEGAE4VgA] [MEAR8gV1KKI] 
    [3kws18e_InQ] [4jtvc716EeM] [Mic4z2oF0bE] [JHAaMt7yAXA] [1FjgYWrpGXs] 
    [6ssXW1hxDRA] [LIMFJTnk6GU] [z805dGhyAtg] [TRr7mYuC7vc] [BnVB6t2fxVg] 
    [B08tv_JVB5k] [1nqRYAVPxPI] [o71MkS4Lsg0] [DTqMgVw4ueY] [fvAc4nDhfVA] 
    [qznN0e7vBO8] [F9Zpc9wccww] [7eULE-qjzpo] [V9j4NSilkoo] [h93LCd6HSIg] 
    [AO8wiOk6Ff8] [zoYrJSLDAns] [zzOun9_1CVA] [PtmQDXFotDw] [E_WW6b6cRpM] 
    [LgNOP_KLvO8] [DlmJhrMjRQU] [BukCurwRSGE] [PWq9VPzvH3U] [7wGWR7hmIjk] 
    [j1uPcjW5OZQ] [719MQRi5G1c] [f_HpaiFmbbs] [zN4dkapPIf8] [H095bWr1D9k] 
    [iRWjrG-7ojc] [3Y-NHh1ACLA] [iN7E0dPGRhQ] [75CvmMijEY4] [rmp2ZidnNsM] 
    [LTHB8yCmzMY] [8NmDgRPGawY] [NL0EvDI3B6w] [I5VyrLdSYHg] [j7IF6RF7SAs] 
    [HAGSWYGkbBA] [UWmpR50zBk4] [j3pHiOwLtYc] [CnBG_FwXhTU] [m1rdsw20lAQ] 
    [kHOPhgUCHjw] [smBXfFbHH28] [ZYaXdz6lCYs] [3pstlCw01jc] [jzML7EjmkvM] 
    [mQsHv8KA8qM] [wUEyKVBHbYw] [WombRvwdVhM] [4L1drSeeYmo] [Z4WcdYgHJuA] 
    [tvp-mFcxp8k] [vkXs7mUhWjw] [axUHZdLK0vM] [qxUJNK4MAzA] [mMi5EIwCw5s] 
    [DW9Rp-cKTMM] [gCRY1Xhp1ek] [OmUwL56DVxQ] [MLNSsBScyyo] [bHicpiFaRv4] 
    [lxkfRJwYjfs] [v7evLoUoRfE] [3Wo1jGHkNHY] [xaEUW4raZRw] [VZjAfj1j3b0] 
    [oTcFN7-xhaQ] [m0c2Unbsk_0] [dah_ZePsrm4] [TlWnOfOQjfg] [NIph0dxRzTY] 
    [DNH3CqMHrD8] [ISVL4QaNWgA] [1hIRcTmMhmg] [8ed2wv7BmD8] [zA0SJdm9xkY] 
    [s721Z6r69t0] [6QvSNpNMgP8] [VRuZ_cuR904] [E8LM7em34f0] [tLNDhu7GQfU] 
    [UcYV_EQZPVY] [kNxZFN55hxM] [if0czJdX8ZQ] [hIfohJgtSGw] [ReZ-bBI3so8] 
    [SOeySwo4Lc0] [efMFiqK0UmM] [W-Uu7A7WDDw] [8RcB5ge6rZw] [L7lbQ42dog4] 
    [sEUk1_4eLRs] [mp5eXnFj-ms] [OhtRpcx7lGw] [o5cymj0t3no] [FR6MHWEZ6Po] 
    [bOUexwNNAt8] [npBT4eHZL9E] [0YSINbqsn5s] [Mu34pLKI7vI] [ZfTbaF3j6q4] 
    [npxIIg5W_9A] [QCb9-SOyPK4] [sjvvSeG2-gY] [QS2bAZh7-5w] [p5l310THb9Y] 
    [ZDxUZM0ap-E] [uvDCjCzqrps] [AeIuH3tf7gM] [SqKSo-dlDwM] [BQlrCi7ALu0] 
    [yzSWqdEDo0o] [UT2p9sltleY] [uPDL1pVjLyE] [paLLCR0IRGs] [xggzUK6_3Mg] 
    [3zXEskrU148] [gsphnAB24Ag] [qs3_N_HukgY] [QfdSi24lFFY] 
    
    DVD 3 (107个视频):
    
    [htkdKTkOhCA] [Wj2MJ-TYQxQ] [FTw2QkEFGWI] [4_M8YdWVdec] [LwmnQJ7B63k] 
    [3ynbMWEOX6s] [kOAnUlYkgGc] [_nzZs08oh-g] [MnHwxvfs288] [gm63RGqqSOI] 
    [0L_moHYhekU] [1jSCS8Y4YaA] [z-9h-vSeIF4] [rMQdAD41Nek] [MDhbXo-dSUo] 
    [YRugYwf-6hY] [R2elxiIbrT4] [UGbjsvBfJmQ] [zCSP9ml7n6k] [fR_tL7fEYYs] 
    [DNwjrLSvFdM] [qlQqpmUeG6k] [QuKiRlQgnFk] [tYA8PTBmetM] [-qkP9Pf6iCU] 
    [3cEzJJS2flI] [riZQayB-j1w] [s64adKLhs9g] [JhaudlvOnSE] [FNcDY5cdxpA] 
    [Dyu0Z3n_O1c] [JZDCAXIb7AY] [NkTeBdmobTo] [SbMlfKFiakg] [N6a_pzHXTAQ] 
    [PxBW9WHz9no] [y__m4NSuX5g] [mTtCafSc-Ng] [6mf7mY5iShg] [fs4CUvEh1qI] 
    [yOPGkP5Lx_4] [lRcexCsdaV0] [Pp9l-K8rQkw] [zrOxjUWC11A] [Gq-yz9zhL9c] 
    [wqUqV_2rFm0] [xcJJWoFM2Uo] [EI670t7ouow] [zZwcm0r9jQE] [mFvrQsuUUAM] 
    [g0CoghXm9x4] [VSq0EZsETSo] [JdLoCTk-SNs] [R7Wotxjv7wY] [icMGrNHg4ks] 
    [K4B9MuI_NBE] [2qY8AEdsy3k] [pjjzfB7Ah_Q] [fri7jjiCtZM] [_iMfi9ckOhU] 
    [60egIsz2hxs] [WY_1Zx_AY3M] [kHm1I11pYYw] [G5koKLgihts] [dkAHPL4mr_M] 
    [YyW81sQdsZA] [B0vSv1mkggw] [hC7UnxQYa1o] [EOEsgQRIAj0] [E0E0kR6Cvzo] 
    [KjTJZHFYNZ8] [J1K0HjDOXPI] [gRsxtBvmzO0] [zRRQEtWQElA] [MewrejdpZtg] 
    [zTqKvJc4eIs] [vRppwAHsohI] [u-HbSfNgPKA] [ulcWPzM4ckg] [uoy2sK_L8z8] 
    [lNPRmXt8z1M] [eh5B1XZx-D4] [Fr4kY5nJ10s] [eqWPVi5GVb4] [B5GmEmYdPfI] 
    [QOIEHzRUnAE] [KKwBw4gsNSo] [pTx0oRt3VbI] [5OKukfiItCY] [HhIDZ5LGDBw] 
    [E4d6lwWRoks] [IToDif64_vY] [mQ0X287pqIM] [GcwkavfpLh0] [eJo81pIGqhw] 
    [UtHrFTQFJ5A] [tdytOGWAm3M] [hTJhTevnyo0] [iX_DjxF5poU] [KTil4UpyL4I] 
    [71DT7Vbc-O4] [EEUZaZaWDBU] [JUuEJMcaKlc] [yedub4M_DCI] [OrAzAm460xs] 
    [wFNX9LwJjNw] [-PsAZk_fn5o] 
    
    DVD 4 (48个视频):
    
    [uycGx9mn3BY] [_cWtJ_VN0J0] [kmGZl_kSzUo] [pJY5sVoH2Cg] [dxneDKgtiZo] 
    [iKRPgLtaHqM] [m6AQLjveUMI] [gRUQlAyffPU] [03os9X8XC2A] [hwIxpjFsT48] 
    [hMv4DG6CvV8] [AjEERP03jHc] [-ak_Gv2T_2M] [RHV-xlm03Og] [er-pYcCDwMY] 
    [j4eqWoGhbCY] [M60LQJfurnI] [41l6RlytVV8] [S9xLeloce0U] [yVRxldwGeZU] 
    [JOoZWZJfnG8] [aM5ZnrK9wMw] [UHasfUIcbk4] [oY5D0VEnIko] [joOpTRzN7Bg] 
    [NZeRFOPuRIg] [FbqxlN3dM2U] [GwEtk0wCFnQ] [44uiYvAPWvY] [M7QGrE_qgcs] 
    [oMBcf6NPI8A] [lwh-6aLj1Bg] [NjAiFO8xlIM] [oN6-6mLNaZA] [qzm8o8rDpyk] 
    [gbUKrmE7ULQ] [My7M6eREP4I] [Df96-sMVrsw] [3CV2xR8wcEs] [UJL1syneFUs] 
    [ndIaA3tLWzE] [ZRORamp_lQk] [iUDLoy-KPsg] [g04NCTqP79k] [LTb-uHQXC_g] 
    [OC_SmT0YwiQ] [_e9G3NVlDWk] [sfL2PCoz14w] 
    
    DVD 5 (76个视频):
    
    [Dx28nETwAWo] [nA1mhtyRByo] [2ngUkAXmXTU] [gQgABfXK-oo] [p8eDOUfvwLk] 
    [UHW-bef-DQs] [BI_FsHkR69k] [BCfRKXRLF3k] [V38Xe2qiSvc] [pEBAfeaF8Jw] 
    [LsNxWZX22tQ] [K_bflF3nquQ] [UyCNZv_-7D0] [Jr4wQ7ADx7c] [uwuhSTKKNAU] 
    [OHt-fPI0tdE] [ztYZ2u25iiE] [0HC7mN-50Xg] [OMAGGPNuitA] [gywJNm2yBtg] 
    [pfAN0mEzRog] [o7YzP5YTbYU] [TC94hZ6Ahfc] [vJY5AqJSIYA] [uznncY9MgLo] 
    [NKihGleyIvQ] [iRM5-o7CJlc] [FBdzvoVUBA8] [9eaHbR1UUh4] [iVi362mTw4g] 
    [0aQSJFNDnSQ] [IqSK9nio-Fg] [2rtrXG1lEE0] [PGwvUH-5ssw] [n_yWR8bcI5c] 
    [xcg3XecUjVI] [yK0swN84Y4A] [JkX57RsDcHA] [V6wghUYifTs] [jsKaHBvsS4c] 
    [a7bl2V6P2HI] [ajovVcbe4-k] [GJcs-0mI1HI] [t1t2suhCaGE] [GUYVrOVoAic] 
    [51HLHqeY-n0] [QXAS9uAM0wQ] [jmxq745o350] [ZQ2FDAGiTjQ] [egRHh0OvqIE] 
    [lfmty1lL3ao] [1yUJ8fL9d7Y] [sqaDO_bNB5Y] [l-QKBHjk0NA] [nMY2DN6uraY] 
    [UoN5HRN4dnM] [QS_CS_ZlHfc] [MQcylZNcnBg] [6ZlxgqBW0og] [k4F7g8aEmeA] 
    [GteOKe8bBdk] [5S1n17vymOg] [LHZSUS2JAg8] [fIDasOCYo-8] [wUUVlUSh-QU] 
    [UUwNK2ODJLw] [Qo9bcRuMs7s] [gMnrwuSKjlY] [2-P9IiWAMTA] [V14nJjEX4d0] 
    [E5G_lxVfDZo] [vt8fo0G15QE] [lnCM3uMCAYk] [3s6dCQ6rMTk] [skk9ynF4XzE] 
    [8covafzMN_8] 
    
    DVD 6 (65个视频):
    
    [6sDvg6vXpeQ] [kpL-Ty8z32Y] [03YODxnl15I] [MxCUt65RTyE] [d8nIn6L1l2o] 
    [A8plod_AiDM] [xJgC8bnC1FE] [4HCbmu5CnTY] [7Sv6IRDMQa4] [uwo8aKc4MTU] 
    [a7F7mfXI2aI] [kja3IzNvG7k] [4Kt6J581zcA] [lIYJgfwrdXw] [6fZ6F8MzCZw] 
    [bLgRcTetwuc] [K_so8PhkWz0] [_HVBhu1jNbw] [xAenI1xdG1Q] [RE5hKzVkELs] 
    [u1of9QAFuY0] [B8IKTA_HfGg] [L5BX2rrRsB8] [yqpyu6EyjiQ] [tR0OyJtWqZc] 
    [ac-JerkzrgU] [gdrto92KyjY] [btBOVPYNQ7Q] [GAPWdyMI1m8] [5IMxorno7rc] 
    [edJ-RJGZr1o] [5gb6g58ythM] [PErBnJx6upU] [viE0PKRdv1A] [8BRcosaBlWA] 
    [OQ1AuhCmty8] [mtWrH0kwTxQ] [F8K1ku6kOsA] [N3LdsewSNS8] [2ONZ7iynSCA] 
    [Df6Jh0FXids] [8hlfmqbe9Lw] [ishO3NyXD8g] [CtUmnz9A2Hw] [P3bUdHg-Mko] 
    [kFMAQGz3o88] [ZKKfObIygsI] [1vWOz8FfKj4] [iU3eCQPa2uM] [epLmXrezS-0] 
    [XPf9gv822_o] [se82U5XBlnM] [MtbqrsJ2vz0] [O8kqp2eQ-cc] [zwhE2Xshpz0] 
    [cYymz1iVTcI] [5xdGKiUFba0] [Tnsfc3K5feU] [WlECXkhsX0k] [_GJV5VOIYTs] 
    [alvUWs6bTiQ] [-635DIxcDxo] [-C4vfk0bLtg] [USN9fh7v1sc] [xRst2zoK39s] 
    
    DVD 7 (81个视频):
    
    [EbbMrNgnBdk] [UtIPzAarHHo] [TuZHSmXE9WI] [oeFP1DaD_FQ] [5q6Shg8u2m4] 
    [nqCPr5ZlbQ0] [Q0c_6u-zkkY] [2WXURR53oQ0] [ct2GQL_1kYo] [OHTj8Tt8wA8] 
    [4deg_9LyWhY] [nhO8l8S8Kww] [WRgQh6GKuUo] [ZzsnICCDmXI] [Gzz4RDPLG9k] 
    [95yHRJQ9G1Q] [Begy1dfuDSU] [N0nOdOKDFZ0] [MK3uct4izGI] [kIdxfedW2yY] 
    [Jkahob7RSuY] [_zZggYcGfYU] [PuC-WuMcyzM] [Fp90lk6TRKQ] [xSLqklG4g_Y] 
    [OtIGINpvuYw] [7AHSFgdbCr8] [xcddc7IJscw] [XaRVooHaDNo] [1x5AB0YsDxU] 
    [IsUP4Ox8RBI] [Tb_GXrjYz4w] [-0Ujzekxcr4] [LsNc7lcDAEk] [n95Ju10ZUjA] 
    [waUhInqf6NU] [owVzeKl5T6g] [AmFBF4tD-vc] [PDdWWlBmIZk] [FWEPOa2Qq2Y] 
    [f4QyCDifgSg] [9w8gP5sUvZY] [bOzLkXYcqi8] [dhE3I0eMQRw] [cBeMpbCMssQ] 
    [GS3dnrFVKNM] [iC-zhwzWseM] [E5Wix65Knjg] [Uvs1m_HlmtU] [6ZeCfvExFOE] 
    [Ap4l3vMLMBM] [mezuCg-9X14] [ds92ofUgZDg] [wiZWSDddIBQ] [M5tvH9WzvYQ] 
    [3kN80eiJuX0] [Fw3xCztscB0] [jlx1fpmScmE] [4yAWcAyOCEs] [ol1P1MS7t68] 
    [3-togXAuYx4] [fwjJTBqf72I] [Cjl2hw-OMTA] [NOJIBhX8FZo] [6-oKmeRZKL0] 
    [TVbdrzNtpUM] [HlCcZ0fqg7E] [cC-YViBEoM0] [BYLgYk72Bsk] [GpX0hjOJ9XE] 
    [Fz5uo4neZB0] [zLcBqF95yGU] [L6TnIVDfJAQ] [H4Q5Pfx73Wc] [WdOM9MqqExY] 
    [YICXSHzP-iU] [aB-kStvtP58] [D2oo1j8mN0o] [3kucN78Ib1Y] [2OYwMpH_ET8] 
    [EBiXUFbE6xw] 
    
    DVD 8 (49个视频):
    
    [GPpVvlDUXh8] [Ii1fNn0MF7k] [Tg6C-jz_6kA] [t0DmSULzWpM] [WHs7kV4fjD0] 
    [lerZE_Gb-h0] [IpUsgoNVkqM] [wPJ2ZhEPDD0] [xkThAttaM5k] [uvEd2VgGxtw] 
    [hsuRQdjHBj0] [fvmYvwLN_eI] [7TwtmVeHTJA] [UhzLJSCZM00] [RrM_zkcn6Q8] 
    [q4Lnd-Z2Ozw] [AEJ-RlkwKyY] [I7ADZT0rbDk] [98ieHtulTVE] [TO71FrSl_iI] 
    [ttTT8dn4J_4] [v3n9A6QxQT0] [btfc46nbtnk] [_ySg4cKrJ6Q] [d1_DbnHf9ak] 
    [FhUW0bVesT0] [4zs1bmGgGiI] [aQnwqmmbOqk] [trAL2Yh7ivk] [nZ0dFeEPG-w] 
    [OJy8US8LHYs] [HgZmu5tZP6k] [1Hia9zHTCcM] [bwwXJHQvliI] [j3SsfJ3ymX0] 
    [JGMPdXLXDmc] [yK_IuNI5tho] [J898UNVh8hI] [GE7SC-nyPCU] [1j9oTpnDUFc] 
    [WGnJYLU9PMk] [FbpNkr1bQSA] [yXAT4QRS7uw] [DK9NEKKyQao] [WRYg8bnMY6I] 
    [hLtE0ftpAik] [wY847yL3pgc] [UP_yEpvdA7A] [2bMqdrXMOTA] 
